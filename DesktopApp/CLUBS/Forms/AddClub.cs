﻿using System;
using System.Linq;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class AddClub : Form
    {
        ctrlClub ClubController;

        public AddClub(ctrlClub ClubController)
        {
            this.ClubController = ClubController;

            InitializeComponent();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            logoPicturebox.ImageLocation = openFileDialog.FileName;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                ClubController.NewClub(nameTextbox.Text, stadiumTextbox.Text, descriptionTextbox.Text, ClubController.BackupLogo(nameTextbox.Text.ToUpper(), logoPicturebox.ImageLocation));
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Verifies if the form is correctly filled
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {
            if(string.IsNullOrEmpty(nameTextbox.Text) || !nameTextbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza um nome valido");
                nameTextbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(stadiumTextbox.Text) || !nameTextbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza um estadio valido");
                stadiumTextbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(descriptionTextbox.Text) || !descriptionTextbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza uma descrição valida");
                descriptionTextbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(logoPicturebox.ImageLocation))
            {
                DialogResult answer = MessageBox.Show("Prentede continuar sem nenhuma imagem?", "Logo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if(answer == DialogResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
