﻿namespace DesktopApp
{
    partial class ClubDetailedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteClubStipButton = new System.Windows.Forms.ToolStripMenuItem();
            this.estatisticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.landingPanel = new System.Windows.Forms.Panel();
            this.restoreClubStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.deleteClubStipButton,
            this.estatisticasToolStripMenuItem,
            this.restoreClubStripMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            this.inicioToolStripMenuItem.Click += new System.EventHandler(this.inicioToolStripMenuItem_Click);
            // 
            // deleteClubStipButton
            // 
            this.deleteClubStipButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.deleteClubStipButton.BackColor = System.Drawing.Color.Red;
            this.deleteClubStipButton.Name = "deleteClubStipButton";
            this.deleteClubStipButton.Size = new System.Drawing.Size(91, 20);
            this.deleteClubStipButton.Text = "Apagar Clube";
            this.deleteClubStipButton.Visible = false;
            this.deleteClubStipButton.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // estatisticasToolStripMenuItem
            // 
            this.estatisticasToolStripMenuItem.Name = "estatisticasToolStripMenuItem";
            this.estatisticasToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.estatisticasToolStripMenuItem.Text = "Estatisticas";
            this.estatisticasToolStripMenuItem.Click += new System.EventHandler(this.estatisticasToolStripMenuItem_Click);
            // 
            // landingPanel
            // 
            this.landingPanel.Location = new System.Drawing.Point(12, 27);
            this.landingPanel.Name = "landingPanel";
            this.landingPanel.Size = new System.Drawing.Size(760, 522);
            this.landingPanel.TabIndex = 3;
            // 
            // restoreClubStripMenu
            // 
            this.restoreClubStripMenu.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.restoreClubStripMenu.BackColor = System.Drawing.Color.Gold;
            this.restoreClubStripMenu.Name = "restoreClubStripMenu";
            this.restoreClubStripMenu.Size = new System.Drawing.Size(106, 20);
            this.restoreClubStripMenu.Text = "Recuperar Clube";
            this.restoreClubStripMenu.Visible = false;
            this.restoreClubStripMenu.Click += new System.EventHandler(this.recuperarClubeToolStripMenuItem_Click);
            // 
            // ClubDetailedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.landingPanel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ClubDetailedForm";
            this.Text = "Informação Clube";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.Panel landingPanel;
        private System.Windows.Forms.ToolStripMenuItem deleteClubStipButton;
        private System.Windows.Forms.ToolStripMenuItem estatisticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreClubStripMenu;
    }
}