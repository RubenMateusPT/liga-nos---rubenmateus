﻿using Library;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class ClubDetailedForm : Form
    {
        private tblClub Club { get; set; }
        private ctrlDatabase Database { get; set; }

        public ClubDetailedForm(ctrlDatabase Database, tblClub Club)
        {
            this.Club = Club;
            this.Database = Database;

            InitializeComponent();

            ClubView();
        }

        /// <summary>
        /// Enables/Disables the Remove/Recover Buttons
        /// </summary>
        private void ClubView()
        {
            landingPanel.Controls.Clear();
            ClubDetailedView uc = new ClubDetailedView(Database, Club);
            landingPanel.Controls.Add(uc);

            if (Club.Enabled)
            {
                deleteClubStipButton.Visible = true;
                deleteClubStipButton.Enabled = true;

                restoreClubStripMenu.Visible = false;
                restoreClubStripMenu.Enabled = false;
            }
            else
            {
                deleteClubStipButton.Visible = false;
                deleteClubStipButton.Enabled = false;

                restoreClubStripMenu.Visible = true;
                restoreClubStripMenu.Enabled = true;
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClubView();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Club.Enabled && CanBeRemoved())
            {
                DialogResult answer = MessageBox.Show("Tem certeza que quer apagar o clube?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (answer == DialogResult.Yes)
                {
                    Database.ClubController.RemoveClub(Club);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("O clube encontrasse de momento a jogar!", "Accção Impossivel",MessageBoxButtons.OK,MessageBoxIcon.Error );
            }
        }

        //Checks if the club can be removed(Disabled)
        private bool CanBeRemoved()
        {
            if(Club.Statistics.Count() == 0)
            {
                return true;
            }
            else if (Club.Statistics.Last().Season.Finished)
            {
                return true;
            }

            return false;
        }

        private void estatisticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            landingPanel.Controls.Clear();
            StatisticsLandingPageUserControl uc = new StatisticsLandingPageUserControl(Database, Club);
            landingPanel.Controls.Add(uc);
        }

        private void recuperarClubeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Club.Enabled)
            {
                Database.ClubController.RestoreClub(Club);
                this.Close();
            }
        }
    }
}
