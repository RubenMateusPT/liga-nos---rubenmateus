﻿namespace DesktopApp
{
    partial class ClubDetailedView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextbox = new System.Windows.Forms.TextBox();
            this.logoPicturebox = new System.Windows.Forms.PictureBox();
            this.stadiumTextbox = new System.Windows.Forms.TextBox();
            this.descriptionTexbox = new System.Windows.Forms.TextBox();
            this.editButoon = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.changeLogoButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lastGamesFlow = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // nameTextbox
            // 
            this.nameTextbox.Enabled = false;
            this.nameTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextbox.Location = new System.Drawing.Point(3, 3);
            this.nameTextbox.Name = "nameTextbox";
            this.nameTextbox.Size = new System.Drawing.Size(448, 40);
            this.nameTextbox.TabIndex = 1;
            // 
            // logoPicturebox
            // 
            this.logoPicturebox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logoPicturebox.Location = new System.Drawing.Point(457, 3);
            this.logoPicturebox.Name = "logoPicturebox";
            this.logoPicturebox.Size = new System.Drawing.Size(300, 300);
            this.logoPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPicturebox.TabIndex = 0;
            this.logoPicturebox.TabStop = false;
            // 
            // stadiumTextbox
            // 
            this.stadiumTextbox.Enabled = false;
            this.stadiumTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stadiumTextbox.Location = new System.Drawing.Point(3, 49);
            this.stadiumTextbox.Name = "stadiumTextbox";
            this.stadiumTextbox.Size = new System.Drawing.Size(448, 31);
            this.stadiumTextbox.TabIndex = 2;
            // 
            // descriptionTexbox
            // 
            this.descriptionTexbox.Enabled = false;
            this.descriptionTexbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descriptionTexbox.Location = new System.Drawing.Point(3, 86);
            this.descriptionTexbox.Multiline = true;
            this.descriptionTexbox.Name = "descriptionTexbox";
            this.descriptionTexbox.Size = new System.Drawing.Size(448, 217);
            this.descriptionTexbox.TabIndex = 3;
            // 
            // editButoon
            // 
            this.editButoon.Location = new System.Drawing.Point(682, 496);
            this.editButoon.Name = "editButoon";
            this.editButoon.Size = new System.Drawing.Size(75, 23);
            this.editButoon.TabIndex = 5;
            this.editButoon.Text = "Edit";
            this.editButoon.UseVisualStyleBackColor = true;
            this.editButoon.Click += new System.EventHandler(this.editButoon_Click);
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(3, 496);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Visible = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Enabled = false;
            this.cancelButton.Location = new System.Drawing.Point(84, 496);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Visible = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // changeLogoButton
            // 
            this.changeLogoButton.Enabled = false;
            this.changeLogoButton.Location = new System.Drawing.Point(682, 3);
            this.changeLogoButton.Name = "changeLogoButton";
            this.changeLogoButton.Size = new System.Drawing.Size(75, 23);
            this.changeLogoButton.TabIndex = 8;
            this.changeLogoButton.Text = "Change";
            this.changeLogoButton.UseVisualStyleBackColor = true;
            this.changeLogoButton.Visible = false;
            this.changeLogoButton.Click += new System.EventHandler(this.changeLogoButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.AddExtension = false;
            this.openFileDialog.DefaultExt = "png;jpg;jpeg";
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "PNG Files | *.png; JPG Images | *.jpg;  JPEG Images | *.jpeg";
            this.openFileDialog.Title = "Logo do Clube";
            // 
            // lastGamesFlow
            // 
            this.lastGamesFlow.AutoScroll = true;
            this.lastGamesFlow.Location = new System.Drawing.Point(3, 309);
            this.lastGamesFlow.Name = "lastGamesFlow";
            this.lastGamesFlow.Size = new System.Drawing.Size(754, 181);
            this.lastGamesFlow.TabIndex = 9;
            // 
            // ClubDetailedView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lastGamesFlow);
            this.Controls.Add(this.changeLogoButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.editButoon);
            this.Controls.Add(this.descriptionTexbox);
            this.Controls.Add(this.stadiumTextbox);
            this.Controls.Add(this.nameTextbox);
            this.Controls.Add(this.logoPicturebox);
            this.Name = "ClubDetailedView";
            this.Size = new System.Drawing.Size(760, 522);
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox logoPicturebox;
        private System.Windows.Forms.TextBox nameTextbox;
        private System.Windows.Forms.TextBox stadiumTextbox;
        private System.Windows.Forms.TextBox descriptionTexbox;
        private System.Windows.Forms.Button editButoon;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button changeLogoButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.FlowLayoutPanel lastGamesFlow;
    }
}
