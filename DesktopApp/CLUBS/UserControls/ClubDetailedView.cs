﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;
using System.Diagnostics;

namespace DesktopApp
{
    public partial class ClubDetailedView : UserControl
    {

        private tblClub Club { get; set; }
        private ctrlDatabase Database { get; set; }

        public ClubDetailedView(ctrlDatabase Database, tblClub Club)
        {
            this.Club = Club;
            this.Database = Database;

            InitializeComponent();

            FormUpdate();
        }
        
        /// <summary>
        /// UPdates the visual of the form
        /// </summary>
        private void FormUpdate()
        {
            nameTextbox.Text = Club.Name;
            nameTextbox.Enabled = false;
            stadiumTextbox.Text = Club.Stadium;
            stadiumTextbox.Enabled = false;
            descriptionTexbox.Text = Club.Description;
            descriptionTexbox.Enabled = false;
            logoPicturebox.ImageLocation = Club.LogoLocation;

            changeLogoButton.Visible = false;
            changeLogoButton.Enabled = false;
            saveButton.Visible = false;
            saveButton.Enabled = false;
            cancelButton.Visible = false;
            cancelButton.Enabled = false;

            
            lastGamesFlow.Controls.Clear();
            foreach(tblSeason season in Database.SeasonController.Seasons)
            {
                foreach(tblFixtures fixture in season.Fixtures)
                {
                    foreach(tblMatch match in fixture.Matches)
                    {
                        if(Club == match.HomeClub || Club == match.AwayClub)
                        {
                            MatchUserControl uc = new MatchUserControl(match);
                            uc.Size = new Size(lastGamesFlow.Size.Width - 25, uc.Height);
                            lastGamesFlow.Controls.Add(uc);
                        }
                    }
                }
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            FormUpdate();
        }

        private void editButoon_Click(object sender, EventArgs e)
        {
            foreach(Control control in this.Controls)
            {
                control.Visible = true;
                control.Enabled = true;
            }
        }

        private void changeLogoButton_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            logoPicturebox.ImageLocation = openFileDialog.FileName;
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                string logolocation = null;

                try
                {
                    logolocation = Database.ClubController.BackupLogo(nameTextbox.Text.ToUpper(), logoPicturebox.ImageLocation);
                }
                catch (Exception)
                {
                    Debug.Write("File already exists\n\nReusing same image");
                    logolocation = Club.LogoLocation;
                
                }
                Database.ClubController.EditClub(Club, nameTextbox.Text, stadiumTextbox.Text, descriptionTexbox.Text, logolocation);

                FormUpdate();

            }
        }

        /// <summary>
        /// Checks if the form is well filled
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {
            foreach(tblClub club in Database.ClubController.ClubList)
            {
                if(club.Name.ToUpper() == nameTextbox.Text.ToUpper() && club != Club)
                {
                    MessageBox.Show("Este clube ja existe!");
                    nameTextbox.Focus();
                    return false;
                }
            }

            if (string.IsNullOrEmpty(nameTextbox.Text) || !nameTextbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza um nome valido");
                nameTextbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(stadiumTextbox.Text) || !nameTextbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza um estadio valido");
                stadiumTextbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(descriptionTexbox.Text) || !descriptionTexbox.Text.Any(char.IsLetter))
            {
                MessageBox.Show("Introduza uma descrição valida");
                descriptionTexbox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(logoPicturebox.ImageLocation))
            {
                DialogResult answer = MessageBox.Show("Prentede continuar sem nenhuma imagem?", "Logo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
