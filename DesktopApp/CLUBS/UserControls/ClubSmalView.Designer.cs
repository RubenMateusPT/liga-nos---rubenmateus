﻿namespace DesktopApp
{
    partial class ClubSmalView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.logoPicturebox = new System.Windows.Forms.PictureBox();
            this.stadiumLabel = new System.Windows.Forms.Label();
            this.detailedButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(153, 3);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(0, 39);
            this.nameLabel.TabIndex = 0;
            // 
            // logoPicturebox
            // 
            this.logoPicturebox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logoPicturebox.Location = new System.Drawing.Point(10, 9);
            this.logoPicturebox.Name = "logoPicturebox";
            this.logoPicturebox.Size = new System.Drawing.Size(130, 130);
            this.logoPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPicturebox.TabIndex = 1;
            this.logoPicturebox.TabStop = false;
            // 
            // stadiumLabel
            // 
            this.stadiumLabel.AutoSize = true;
            this.stadiumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stadiumLabel.Location = new System.Drawing.Point(156, 50);
            this.stadiumLabel.Name = "stadiumLabel";
            this.stadiumLabel.Size = new System.Drawing.Size(0, 20);
            this.stadiumLabel.TabIndex = 2;
            // 
            // detailedButton
            // 
            this.detailedButton.Location = new System.Drawing.Point(611, 33);
            this.detailedButton.Name = "detailedButton";
            this.detailedButton.Size = new System.Drawing.Size(75, 75);
            this.detailedButton.TabIndex = 4;
            this.detailedButton.Text = "Mais detalhes";
            this.detailedButton.UseVisualStyleBackColor = true;
            this.detailedButton.Click += new System.EventHandler(this.detailedButton_Click);
            // 
            // ClubSmalView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.detailedButton);
            this.Controls.Add(this.stadiumLabel);
            this.Controls.Add(this.logoPicturebox);
            this.Controls.Add(this.nameLabel);
            this.Name = "ClubSmalView";
            this.Size = new System.Drawing.Size(700, 148);
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.PictureBox logoPicturebox;
        private System.Windows.Forms.Label stadiumLabel;
        private System.Windows.Forms.Button detailedButton;
    }
}
