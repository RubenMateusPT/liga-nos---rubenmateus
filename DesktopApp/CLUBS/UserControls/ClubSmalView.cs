﻿using Library;
using System;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class ClubSmalView : UserControl
    {
        private tblClub Club { get; set; }
        private ctrlDatabase Database { get; set; }
        private ClubsLandingPage UserControl { get; set; }

        public ClubSmalView(ClubsLandingPage uc,ctrlDatabase Database, tblClub Club)
        {
            this.Club = Club;
            this.Database = Database;
            this.UserControl = uc;

            InitializeComponent();

            FormUpdate();
        }

        private void detailedButton_Click(object sender, EventArgs e)
        {
            ClubDetailedForm frm = new ClubDetailedForm(Database, Club);
            frm.ShowDialog();
            FormUpdate();
            UserControl.FormUpdate();
        }


        /// <summary>
        /// Updates the visual of the form
        /// </summary>
        private void FormUpdate()
        {

            nameLabel.Text = Club.Name;
            stadiumLabel.Text = Club.Stadium;
            logoPicturebox.ImageLocation = Club.LogoLocation;

            if (Club.Enabled)
            {
                foreach(Control control in this.Controls)
                {
                    control.Enabled = true;
                }
            }
            else
            {
                foreach (Control control in this.Controls)
                {
                    control.Enabled = false;
                }
            }

            detailedButton.Enabled = true;

        }
    }
}
