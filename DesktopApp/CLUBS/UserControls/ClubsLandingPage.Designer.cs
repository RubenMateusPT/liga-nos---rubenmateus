﻿namespace DesktopApp
{
    partial class ClubsLandingPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usercontrolClubPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.newClubButton = new System.Windows.Forms.Button();
            this.detailedButton = new System.Windows.Forms.Button();
            this.stadiumLabel = new System.Windows.Forms.Label();
            this.logoPicturebox = new System.Windows.Forms.PictureBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // usercontrolClubPanel
            // 
            this.usercontrolClubPanel.AutoScroll = true;
            this.usercontrolClubPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.usercontrolClubPanel.Location = new System.Drawing.Point(3, 159);
            this.usercontrolClubPanel.Name = "usercontrolClubPanel";
            this.usercontrolClubPanel.Size = new System.Drawing.Size(744, 360);
            this.usercontrolClubPanel.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.newClubButton);
            this.groupBox1.Controls.Add(this.detailedButton);
            this.groupBox1.Controls.Add(this.stadiumLabel);
            this.groupBox1.Controls.Add(this.logoPicturebox);
            this.groupBox1.Controls.Add(this.nameLabel);
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(744, 150);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // newClubButton
            // 
            this.newClubButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newClubButton.Location = new System.Drawing.Point(224, 19);
            this.newClubButton.Name = "newClubButton";
            this.newClubButton.Size = new System.Drawing.Size(324, 113);
            this.newClubButton.TabIndex = 9;
            this.newClubButton.Text = "Adicionar Clube";
            this.newClubButton.UseVisualStyleBackColor = true;
            this.newClubButton.Click += new System.EventHandler(this.newClubButton_Click);
            // 
            // detailedButton
            // 
            this.detailedButton.Enabled = false;
            this.detailedButton.Location = new System.Drawing.Point(662, 41);
            this.detailedButton.Name = "detailedButton";
            this.detailedButton.Size = new System.Drawing.Size(75, 75);
            this.detailedButton.TabIndex = 8;
            this.detailedButton.Text = "Mais detalhes";
            this.detailedButton.UseVisualStyleBackColor = true;
            // 
            // stadiumLabel
            // 
            this.stadiumLabel.AutoSize = true;
            this.stadiumLabel.Enabled = false;
            this.stadiumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stadiumLabel.Location = new System.Drawing.Point(162, 61);
            this.stadiumLabel.Name = "stadiumLabel";
            this.stadiumLabel.Size = new System.Drawing.Size(129, 20);
            this.stadiumLabel.TabIndex = 7;
            this.stadiumLabel.Text = "Nome do estadio";
            // 
            // logoPicturebox
            // 
            this.logoPicturebox.Enabled = false;
            this.logoPicturebox.Image = global::DesktopApp.Properties.Resources.Liga_NOS_logo;
            this.logoPicturebox.InitialImage = global::DesktopApp.Properties.Resources.Liga_NOS_logo;
            this.logoPicturebox.Location = new System.Drawing.Point(11, 11);
            this.logoPicturebox.Name = "logoPicturebox";
            this.logoPicturebox.Size = new System.Drawing.Size(130, 130);
            this.logoPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPicturebox.TabIndex = 6;
            this.logoPicturebox.TabStop = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Enabled = false;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(160, 14);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(262, 39);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Nome do clube";
            // 
            // ClubsLandingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.usercontrolClubPanel);
            this.Name = "ClubsLandingPage";
            this.Size = new System.Drawing.Size(760, 522);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel usercontrolClubPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button detailedButton;
        private System.Windows.Forms.Label stadiumLabel;
        private System.Windows.Forms.PictureBox logoPicturebox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button newClubButton;
    }
}
