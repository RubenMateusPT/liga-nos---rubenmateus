﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class ClubsLandingPage : UserControl
    {
        private ctrlDatabase Database { get; set; }
        private ctrlClub ClubController { get; set; }

        public ClubsLandingPage(ctrlDatabase Database)
        {
            this.Database = Database;
            this.ClubController = Database.ClubController;

            InitializeComponent();

            FormUpdate();
        }

        /// <summary>
        /// Updates the visual of the form, Based on the active and inactive clubs
        /// </summary>
        public void FormUpdate()
        {
            int activeClubs = 0;
            if (Database.ClubController.ClubList.Count == 0)
            {
                MessageBox.Show("Não existem clubes na base de dado!\n\n Crie clubes primeiro");
            }
            else
            {
                usercontrolClubPanel.Controls.Clear();

                Label label = new Label();
                label.Text = "Clubes Ativos";
                label.Size = new Size(usercontrolClubPanel.Width-30, 35);
                label.Font = new Font(FontFamily.GenericMonospace, 15);
                label.TextAlign = ContentAlignment.MiddleCenter;
                usercontrolClubPanel.Controls.Add(label);

                
                foreach (tblClub club in Database.ClubController.ClubList)
                {
                    if (club.Enabled)
                    {
                        ClubSmalView newClubUc = new ClubSmalView(this, Database, club);                        
                        usercontrolClubPanel.Controls.Add(newClubUc);
                        activeClubs++;
                    }
                }

                label = new Label();
                label.Text = "Clubes Inativos";
                label.Size = new Size(usercontrolClubPanel.Width-30, 35);
                label.Font = new Font(FontFamily.GenericMonospace, 15);
                label.TextAlign = ContentAlignment.MiddleCenter;
                usercontrolClubPanel.Controls.Add(label);

                foreach (tblClub club in Database.ClubController.ClubList)
                {
                    if (!club.Enabled)
                    {
                        ClubSmalView newClubUc = new ClubSmalView(this, Database, club);
                        usercontrolClubPanel.Controls.Add(newClubUc);
                    }
                }

            }

            newClubButton.Text = $"Adicionar Clube\n({activeClubs.ToString()} / {Database.ClubController.ClubList.Count().ToString()})";
        }

        private void newClubButton_Click(object sender, EventArgs e)
        {
            AddClub frm = new AddClub(ClubController);
            frm.ShowDialog();
            FormUpdate();
        }
    }
}
