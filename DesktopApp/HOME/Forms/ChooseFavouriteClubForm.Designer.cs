﻿namespace DesktopApp
{
    partial class ChooseFavouriteClubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clubListCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.clubChoosenLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.clubChoosenLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // clubListCombobox
            // 
            this.clubListCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clubListCombobox.FormattingEnabled = true;
            this.clubListCombobox.Location = new System.Drawing.Point(16, 78);
            this.clubListCombobox.Name = "clubListCombobox";
            this.clubListCombobox.Size = new System.Drawing.Size(306, 39);
            this.clubListCombobox.TabIndex = 0;
            this.clubListCombobox.SelectedValueChanged += new System.EventHandler(this.clubListCombobox_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clubes Disponiveis:";
            // 
            // clubChoosenLogoPictureBox
            // 
            this.clubChoosenLogoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clubChoosenLogoPictureBox.Location = new System.Drawing.Point(345, 9);
            this.clubChoosenLogoPictureBox.Name = "clubChoosenLogoPictureBox";
            this.clubChoosenLogoPictureBox.Size = new System.Drawing.Size(175, 175);
            this.clubChoosenLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.clubChoosenLogoPictureBox.TabIndex = 2;
            this.clubChoosenLogoPictureBox.TabStop = false;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(345, 190);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Confirmar";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(445, 190);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancelar";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // ChooseFavouriteClubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 221);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.clubChoosenLogoPictureBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clubListCombobox);
            this.Name = "ChooseFavouriteClubForm";
            this.Text = "Escolher Clube Favourito";
            ((System.ComponentModel.ISupportInitialize)(this.clubChoosenLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox clubListCombobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox clubChoosenLogoPictureBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
    }
}