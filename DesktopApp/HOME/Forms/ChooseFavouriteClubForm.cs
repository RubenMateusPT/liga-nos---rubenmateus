﻿using Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class ChooseFavouriteClubForm : Form
    {
        public ctrlDatabase Database { get; set; }

        public ChooseFavouriteClubForm(ctrlDatabase Database)
        {
            this.Database = Database;

            InitializeComponent();

            FormUpdate();
        }

        /// <summary>
        /// Updates the visual of the form
        /// </summary>
        private void FormUpdate()
        {
            clubListCombobox.Items.Clear();

            foreach(tblClub club in Database.ClubController.ClubList)
            {
                clubListCombobox.Items.Add(club);
                clubListCombobox.DisplayMember = "Name";
            }
        }

        

        private void clubListCombobox_SelectedValueChanged(object sender, EventArgs e)
        {
            tblClub clubChoosen = (tblClub)clubListCombobox.SelectedItem;

            clubChoosenLogoPictureBox.ImageLocation = clubChoosen.LogoLocation;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            //SAves the choice to user
            Database.UserController.Users.ElementAt(0).FavouriteClub = (tblClub)clubListCombobox.SelectedItem;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
