﻿namespace DesktopApp
{
    partial class ClubClassificationTableUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.classificationLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.logoPicturebox = new System.Windows.Forms.PictureBox();
            this.gamesplayedLabel = new System.Windows.Forms.Label();
            this.victoriesLabel = new System.Windows.Forms.Label();
            this.drawLabel = new System.Windows.Forms.Label();
            this.defeatLabel = new System.Windows.Forms.Label();
            this.goalsScoredLabel = new System.Windows.Forms.Label();
            this.goalsConcededLabel = new System.Windows.Forms.Label();
            this.pointsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // classificationLabel
            // 
            this.classificationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classificationLabel.Location = new System.Drawing.Point(3, 3);
            this.classificationLabel.Name = "classificationLabel";
            this.classificationLabel.Size = new System.Drawing.Size(41, 25);
            this.classificationLabel.TabIndex = 0;
            this.classificationLabel.Text = "10º";
            this.classificationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoEllipsis = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(81, 3);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(121, 25);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Portimonense";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // logoPicturebox
            // 
            this.logoPicturebox.Image = global::DesktopApp.Properties.Resources.Liga_NOS_logo;
            this.logoPicturebox.Location = new System.Drawing.Point(50, 2);
            this.logoPicturebox.Name = "logoPicturebox";
            this.logoPicturebox.Size = new System.Drawing.Size(25, 25);
            this.logoPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPicturebox.TabIndex = 2;
            this.logoPicturebox.TabStop = false;
            // 
            // gamesplayedLabel
            // 
            this.gamesplayedLabel.AutoEllipsis = true;
            this.gamesplayedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gamesplayedLabel.Location = new System.Drawing.Point(208, 3);
            this.gamesplayedLabel.Name = "gamesplayedLabel";
            this.gamesplayedLabel.Size = new System.Drawing.Size(38, 25);
            this.gamesplayedLabel.TabIndex = 3;
            this.gamesplayedLabel.Text = "50";
            this.gamesplayedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // victoriesLabel
            // 
            this.victoriesLabel.AutoEllipsis = true;
            this.victoriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.victoriesLabel.Location = new System.Drawing.Point(252, 3);
            this.victoriesLabel.Name = "victoriesLabel";
            this.victoriesLabel.Size = new System.Drawing.Size(36, 25);
            this.victoriesLabel.TabIndex = 4;
            this.victoriesLabel.Text = "50";
            this.victoriesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // drawLabel
            // 
            this.drawLabel.AutoEllipsis = true;
            this.drawLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drawLabel.Location = new System.Drawing.Point(294, 3);
            this.drawLabel.Name = "drawLabel";
            this.drawLabel.Size = new System.Drawing.Size(36, 25);
            this.drawLabel.TabIndex = 5;
            this.drawLabel.Text = "50";
            this.drawLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // defeatLabel
            // 
            this.defeatLabel.AutoEllipsis = true;
            this.defeatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defeatLabel.Location = new System.Drawing.Point(336, 3);
            this.defeatLabel.Name = "defeatLabel";
            this.defeatLabel.Size = new System.Drawing.Size(36, 25);
            this.defeatLabel.TabIndex = 6;
            this.defeatLabel.Text = "50";
            this.defeatLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // goalsScoredLabel
            // 
            this.goalsScoredLabel.AutoEllipsis = true;
            this.goalsScoredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goalsScoredLabel.Location = new System.Drawing.Point(378, 3);
            this.goalsScoredLabel.Name = "goalsScoredLabel";
            this.goalsScoredLabel.Size = new System.Drawing.Size(36, 25);
            this.goalsScoredLabel.TabIndex = 7;
            this.goalsScoredLabel.Text = "50";
            this.goalsScoredLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // goalsConcededLabel
            // 
            this.goalsConcededLabel.AutoEllipsis = true;
            this.goalsConcededLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goalsConcededLabel.Location = new System.Drawing.Point(420, 3);
            this.goalsConcededLabel.Name = "goalsConcededLabel";
            this.goalsConcededLabel.Size = new System.Drawing.Size(36, 25);
            this.goalsConcededLabel.TabIndex = 8;
            this.goalsConcededLabel.Text = "50";
            this.goalsConcededLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pointsLabel
            // 
            this.pointsLabel.AutoEllipsis = true;
            this.pointsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pointsLabel.Location = new System.Drawing.Point(462, 3);
            this.pointsLabel.Name = "pointsLabel";
            this.pointsLabel.Size = new System.Drawing.Size(36, 25);
            this.pointsLabel.TabIndex = 9;
            this.pointsLabel.Text = "50";
            this.pointsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ClubClassificationTableUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.pointsLabel);
            this.Controls.Add(this.goalsConcededLabel);
            this.Controls.Add(this.goalsScoredLabel);
            this.Controls.Add(this.defeatLabel);
            this.Controls.Add(this.drawLabel);
            this.Controls.Add(this.victoriesLabel);
            this.Controls.Add(this.gamesplayedLabel);
            this.Controls.Add(this.logoPicturebox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.classificationLabel);
            this.Name = "ClubClassificationTableUserControl";
            this.Size = new System.Drawing.Size(500, 30);
            ((System.ComponentModel.ISupportInitialize)(this.logoPicturebox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label classificationLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.PictureBox logoPicturebox;
        private System.Windows.Forms.Label gamesplayedLabel;
        private System.Windows.Forms.Label victoriesLabel;
        private System.Windows.Forms.Label drawLabel;
        private System.Windows.Forms.Label defeatLabel;
        private System.Windows.Forms.Label goalsScoredLabel;
        private System.Windows.Forms.Label goalsConcededLabel;
        private System.Windows.Forms.Label pointsLabel;
    }
}
