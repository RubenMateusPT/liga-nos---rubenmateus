﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class ClubClassificationTableUserControl : UserControl
    {
        private tblClub Club { get; set; }
        private ctrlDatabase Database { get; set; }
        private tblSeason CurrentSeason { get; set; }

        public ClubClassificationTableUserControl(tblSeason CurrentSeason,tblClub Club)
        {
            this.Club = Club;
            this.CurrentSeason = CurrentSeason;

            InitializeComponent();

            FormUpdate();
        }

        /// <summary>
        /// Updates the visual of the form
        /// </summary>
        private void FormUpdate()
        {
            logoPicturebox.ImageLocation = Club.LogoLocation;
            nameLabel.Text = Club.Name;

            tblStatistics CurrentStats = null;

            foreach (tblStatistics stat in Club.Statistics)
            {
                if(CurrentSeason == stat.Season)
                {
                     CurrentStats = stat;
                }
            }
            

            classificationLabel.Text = CurrentStats.Classification.ToString();
            gamesplayedLabel.Text = CurrentStats.GamesPlayed.ToString();
            victoriesLabel.Text = CurrentStats.GamesWon.ToString();
            drawLabel.Text = CurrentStats.GamesTied.ToString();
            defeatLabel.Text = CurrentStats.GamesLost.ToString();
            goalsScoredLabel.Text = CurrentStats.GoalsScored.ToString();
            goalsConcededLabel.Text = CurrentStats.GoalsConceded.ToString();
            pointsLabel.Text = CurrentStats.SeasonPoints.ToString();


        }
    }
}
