﻿namespace DesktopApp
{
    partial class LandingHomePage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.favouriteClubGroupbox = new System.Windows.Forms.GroupBox();
            this.appControlButtonsGroupbox = new System.Windows.Forms.GroupBox();
            this.createSeasonButton = new System.Windows.Forms.Button();
            this.classificationPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.clublogoPictureBox = new System.Windows.Forms.PictureBox();
            this.clubNameLabel = new System.Windows.Forms.Label();
            this.currentFixtureGamesFlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.currentFixutreLabel = new System.Windows.Forms.Label();
            this.emulateFixtureButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.seasonInfoGroupbox = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CurrentSeasonNumberLabel = new System.Windows.Forms.Label();
            this.TotalSeasonFixturesLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TotalSeasonGamesLabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.currentSeasonFinisehdLabel = new System.Windows.Forms.Label();
            this.changeFavouritecClubButton = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.clubInSeasonLabel = new System.Windows.Forms.Label();
            this.clubSeasonGroupbox = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.clubCurrentPlaceLabel = new System.Windows.Forms.Label();
            this.clubCurrentPointsLabel = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ClubCurrentVictoriesLabel = new System.Windows.Forms.Label();
            this.favouriteClubGroupbox.SuspendLayout();
            this.appControlButtonsGroupbox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clublogoPictureBox)).BeginInit();
            this.seasonInfoGroupbox.SuspendLayout();
            this.clubSeasonGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // favouriteClubGroupbox
            // 
            this.favouriteClubGroupbox.Controls.Add(this.clubSeasonGroupbox);
            this.favouriteClubGroupbox.Controls.Add(this.clubInSeasonLabel);
            this.favouriteClubGroupbox.Controls.Add(this.label22);
            this.favouriteClubGroupbox.Controls.Add(this.clubNameLabel);
            this.favouriteClubGroupbox.Controls.Add(this.clublogoPictureBox);
            this.favouriteClubGroupbox.Location = new System.Drawing.Point(3, 5);
            this.favouriteClubGroupbox.Name = "favouriteClubGroupbox";
            this.favouriteClubGroupbox.Size = new System.Drawing.Size(200, 258);
            this.favouriteClubGroupbox.TabIndex = 1;
            this.favouriteClubGroupbox.TabStop = false;
            this.favouriteClubGroupbox.Text = "Meu Clube";
            // 
            // appControlButtonsGroupbox
            // 
            this.appControlButtonsGroupbox.Controls.Add(this.changeFavouritecClubButton);
            this.appControlButtonsGroupbox.Controls.Add(this.emulateFixtureButton);
            this.appControlButtonsGroupbox.Controls.Add(this.createSeasonButton);
            this.appControlButtonsGroupbox.Location = new System.Drawing.Point(3, 417);
            this.appControlButtonsGroupbox.Name = "appControlButtonsGroupbox";
            this.appControlButtonsGroupbox.Size = new System.Drawing.Size(754, 100);
            this.appControlButtonsGroupbox.TabIndex = 2;
            this.appControlButtonsGroupbox.TabStop = false;
            this.appControlButtonsGroupbox.Text = "Controlos";
            // 
            // createSeasonButton
            // 
            this.createSeasonButton.Enabled = false;
            this.createSeasonButton.Location = new System.Drawing.Point(6, 19);
            this.createSeasonButton.Name = "createSeasonButton";
            this.createSeasonButton.Size = new System.Drawing.Size(121, 75);
            this.createSeasonButton.TabIndex = 0;
            this.createSeasonButton.Text = "Nova Epoca";
            this.createSeasonButton.UseVisualStyleBackColor = true;
            this.createSeasonButton.Click += new System.EventHandler(this.createSeasonButton_Click);
            // 
            // classificationPanel
            // 
            this.classificationPanel.AutoScroll = true;
            this.classificationPanel.Location = new System.Drawing.Point(224, 45);
            this.classificationPanel.Name = "classificationPanel";
            this.classificationPanel.Size = new System.Drawing.Size(533, 218);
            this.classificationPanel.TabIndex = 3;
            this.classificationPanel.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(228, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(501, 40);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "POS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Equipa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "JJ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "V";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(349, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "D";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(306, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "E";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(385, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "GM";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "GS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(470, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Pts";
            // 
            // clublogoPictureBox
            // 
            this.clublogoPictureBox.BackColor = System.Drawing.Color.Silver;
            this.clublogoPictureBox.Location = new System.Drawing.Point(55, 19);
            this.clublogoPictureBox.Name = "clublogoPictureBox";
            this.clublogoPictureBox.Size = new System.Drawing.Size(90, 90);
            this.clublogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.clublogoPictureBox.TabIndex = 5;
            this.clublogoPictureBox.TabStop = false;
            // 
            // clubNameLabel
            // 
            this.clubNameLabel.AutoEllipsis = true;
            this.clubNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clubNameLabel.Location = new System.Drawing.Point(6, 112);
            this.clubNameLabel.Name = "clubNameLabel";
            this.clubNameLabel.Size = new System.Drawing.Size(188, 23);
            this.clubNameLabel.TabIndex = 5;
            this.clubNameLabel.Text = "-";
            this.clubNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentFixtureGamesFlowLayout
            // 
            this.currentFixtureGamesFlowLayout.AutoScroll = true;
            this.currentFixtureGamesFlowLayout.Location = new System.Drawing.Point(224, 282);
            this.currentFixtureGamesFlowLayout.Name = "currentFixtureGamesFlowLayout";
            this.currentFixtureGamesFlowLayout.Size = new System.Drawing.Size(533, 129);
            this.currentFixtureGamesFlowLayout.TabIndex = 4;
            this.currentFixtureGamesFlowLayout.Visible = false;
            // 
            // currentFixutreLabel
            // 
            this.currentFixutreLabel.AutoSize = true;
            this.currentFixutreLabel.Location = new System.Drawing.Point(317, 266);
            this.currentFixutreLabel.Name = "currentFixutreLabel";
            this.currentFixutreLabel.Size = new System.Drawing.Size(25, 13);
            this.currentFixutreLabel.TabIndex = 6;
            this.currentFixutreLabel.Text = "1/X";
            this.currentFixutreLabel.Visible = false;
            // 
            // emulateFixtureButton
            // 
            this.emulateFixtureButton.Enabled = false;
            this.emulateFixtureButton.Location = new System.Drawing.Point(142, 19);
            this.emulateFixtureButton.Name = "emulateFixtureButton";
            this.emulateFixtureButton.Size = new System.Drawing.Size(121, 75);
            this.emulateFixtureButton.TabIndex = 1;
            this.emulateFixtureButton.Text = "Simular Jornada";
            this.emulateFixtureButton.UseVisualStyleBackColor = true;
            this.emulateFixtureButton.Click += new System.EventHandler(this.emulateFixtureButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(221, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Jogos da Jornada:";
            this.label11.Visible = false;
            // 
            // seasonInfoGroupbox
            // 
            this.seasonInfoGroupbox.Controls.Add(this.currentSeasonFinisehdLabel);
            this.seasonInfoGroupbox.Controls.Add(this.label19);
            this.seasonInfoGroupbox.Controls.Add(this.TotalSeasonGamesLabel);
            this.seasonInfoGroupbox.Controls.Add(this.label17);
            this.seasonInfoGroupbox.Controls.Add(this.TotalSeasonFixturesLabel);
            this.seasonInfoGroupbox.Controls.Add(this.label16);
            this.seasonInfoGroupbox.Controls.Add(this.CurrentSeasonNumberLabel);
            this.seasonInfoGroupbox.Controls.Add(this.label13);
            this.seasonInfoGroupbox.Location = new System.Drawing.Point(3, 269);
            this.seasonInfoGroupbox.Name = "seasonInfoGroupbox";
            this.seasonInfoGroupbox.Size = new System.Drawing.Size(200, 142);
            this.seasonInfoGroupbox.TabIndex = 6;
            this.seasonInfoGroupbox.TabStop = false;
            this.seasonInfoGroupbox.Text = "Informação Epoca";
            this.seasonInfoGroupbox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Epoca nº:";
            // 
            // CurrentSeasonNumberLabel
            // 
            this.CurrentSeasonNumberLabel.AutoSize = true;
            this.CurrentSeasonNumberLabel.Location = new System.Drawing.Point(92, 36);
            this.CurrentSeasonNumberLabel.Name = "CurrentSeasonNumberLabel";
            this.CurrentSeasonNumberLabel.Size = new System.Drawing.Size(13, 13);
            this.CurrentSeasonNumberLabel.TabIndex = 7;
            this.CurrentSeasonNumberLabel.Text = "0";
            // 
            // TotalSeasonFixturesLabel
            // 
            this.TotalSeasonFixturesLabel.AutoSize = true;
            this.TotalSeasonFixturesLabel.Location = new System.Drawing.Point(92, 61);
            this.TotalSeasonFixturesLabel.Name = "TotalSeasonFixturesLabel";
            this.TotalSeasonFixturesLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalSeasonFixturesLabel.TabIndex = 8;
            this.TotalSeasonFixturesLabel.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Nº Jornadas:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Nº Jogos:";
            // 
            // TotalSeasonGamesLabel
            // 
            this.TotalSeasonGamesLabel.AutoSize = true;
            this.TotalSeasonGamesLabel.Location = new System.Drawing.Point(92, 86);
            this.TotalSeasonGamesLabel.Name = "TotalSeasonGamesLabel";
            this.TotalSeasonGamesLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalSeasonGamesLabel.TabIndex = 11;
            this.TotalSeasonGamesLabel.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(25, 112);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Terminada:";
            // 
            // currentSeasonFinisehdLabel
            // 
            this.currentSeasonFinisehdLabel.AutoSize = true;
            this.currentSeasonFinisehdLabel.Location = new System.Drawing.Point(92, 112);
            this.currentSeasonFinisehdLabel.Name = "currentSeasonFinisehdLabel";
            this.currentSeasonFinisehdLabel.Size = new System.Drawing.Size(27, 13);
            this.currentSeasonFinisehdLabel.TabIndex = 13;
            this.currentSeasonFinisehdLabel.Text = "Não";
            // 
            // changeFavouritecClubButton
            // 
            this.changeFavouritecClubButton.Location = new System.Drawing.Point(627, 19);
            this.changeFavouritecClubButton.Name = "changeFavouritecClubButton";
            this.changeFavouritecClubButton.Size = new System.Drawing.Size(121, 75);
            this.changeFavouritecClubButton.TabIndex = 2;
            this.changeFavouritecClubButton.Text = "Mudar Meu Clube";
            this.changeFavouritecClubButton.UseVisualStyleBackColor = true;
            this.changeFavouritecClubButton.Click += new System.EventHandler(this.changeFavouritecClubButton_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 143);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 15;
            this.label22.Text = "Esta na Epoca?:";
            // 
            // clubInSeasonLabel
            // 
            this.clubInSeasonLabel.AutoSize = true;
            this.clubInSeasonLabel.Location = new System.Drawing.Point(95, 143);
            this.clubInSeasonLabel.Name = "clubInSeasonLabel";
            this.clubInSeasonLabel.Size = new System.Drawing.Size(27, 13);
            this.clubInSeasonLabel.TabIndex = 14;
            this.clubInSeasonLabel.Text = "Não";
            // 
            // clubSeasonGroupbox
            // 
            this.clubSeasonGroupbox.Controls.Add(this.ClubCurrentVictoriesLabel);
            this.clubSeasonGroupbox.Controls.Add(this.label27);
            this.clubSeasonGroupbox.Controls.Add(this.clubCurrentPointsLabel);
            this.clubSeasonGroupbox.Controls.Add(this.clubCurrentPlaceLabel);
            this.clubSeasonGroupbox.Controls.Add(this.label25);
            this.clubSeasonGroupbox.Controls.Add(this.label21);
            this.clubSeasonGroupbox.Location = new System.Drawing.Point(6, 159);
            this.clubSeasonGroupbox.Name = "clubSeasonGroupbox";
            this.clubSeasonGroupbox.Size = new System.Drawing.Size(188, 93);
            this.clubSeasonGroupbox.TabIndex = 0;
            this.clubSeasonGroupbox.TabStop = false;
            this.clubSeasonGroupbox.Text = "Epoca";
            this.clubSeasonGroupbox.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 16;
            this.label21.Text = "Classificação:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(37, 43);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 18;
            this.label25.Text = "Pontos:";
            // 
            // clubCurrentPlaceLabel
            // 
            this.clubCurrentPlaceLabel.AutoSize = true;
            this.clubCurrentPlaceLabel.Location = new System.Drawing.Point(89, 18);
            this.clubCurrentPlaceLabel.Name = "clubCurrentPlaceLabel";
            this.clubCurrentPlaceLabel.Size = new System.Drawing.Size(13, 13);
            this.clubCurrentPlaceLabel.TabIndex = 14;
            this.clubCurrentPlaceLabel.Text = "0";
            // 
            // clubCurrentPointsLabel
            // 
            this.clubCurrentPointsLabel.AutoSize = true;
            this.clubCurrentPointsLabel.Location = new System.Drawing.Point(89, 43);
            this.clubCurrentPointsLabel.Name = "clubCurrentPointsLabel";
            this.clubCurrentPointsLabel.Size = new System.Drawing.Size(13, 13);
            this.clubCurrentPointsLabel.TabIndex = 19;
            this.clubCurrentPointsLabel.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(37, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 13);
            this.label27.TabIndex = 20;
            this.label27.Text = "Vitorias:";
            // 
            // ClubCurrentVictoriesLabel
            // 
            this.ClubCurrentVictoriesLabel.AutoSize = true;
            this.ClubCurrentVictoriesLabel.Location = new System.Drawing.Point(89, 67);
            this.ClubCurrentVictoriesLabel.Name = "ClubCurrentVictoriesLabel";
            this.ClubCurrentVictoriesLabel.Size = new System.Drawing.Size(13, 13);
            this.ClubCurrentVictoriesLabel.TabIndex = 21;
            this.ClubCurrentVictoriesLabel.Text = "0";
            // 
            // LandingHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.seasonInfoGroupbox);
            this.Controls.Add(this.currentFixutreLabel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.currentFixtureGamesFlowLayout);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.classificationPanel);
            this.Controls.Add(this.appControlButtonsGroupbox);
            this.Controls.Add(this.favouriteClubGroupbox);
            this.Name = "LandingHomePage";
            this.Size = new System.Drawing.Size(760, 520);
            this.favouriteClubGroupbox.ResumeLayout(false);
            this.favouriteClubGroupbox.PerformLayout();
            this.appControlButtonsGroupbox.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clublogoPictureBox)).EndInit();
            this.seasonInfoGroupbox.ResumeLayout(false);
            this.seasonInfoGroupbox.PerformLayout();
            this.clubSeasonGroupbox.ResumeLayout(false);
            this.clubSeasonGroupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox favouriteClubGroupbox;
        private System.Windows.Forms.GroupBox appControlButtonsGroupbox;
        private System.Windows.Forms.Button createSeasonButton;
        private System.Windows.Forms.FlowLayoutPanel classificationPanel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox clubSeasonGroupbox;
        private System.Windows.Forms.Label ClubCurrentVictoriesLabel;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label clubCurrentPointsLabel;
        private System.Windows.Forms.Label clubCurrentPlaceLabel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label clubInSeasonLabel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label clubNameLabel;
        private System.Windows.Forms.PictureBox clublogoPictureBox;
        private System.Windows.Forms.Button changeFavouritecClubButton;
        private System.Windows.Forms.Button emulateFixtureButton;
        private System.Windows.Forms.FlowLayoutPanel currentFixtureGamesFlowLayout;
        private System.Windows.Forms.Label currentFixutreLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox seasonInfoGroupbox;
        private System.Windows.Forms.Label currentSeasonFinisehdLabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label TotalSeasonGamesLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label TotalSeasonFixturesLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label CurrentSeasonNumberLabel;
        private System.Windows.Forms.Label label13;
    }
}
