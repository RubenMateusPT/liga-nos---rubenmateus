﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;
using System.Threading;

namespace DesktopApp
{
    public partial class LandingHomePage : UserControl
    {
        private ctrlDatabase Database { get; set; }
        private tblSeason CurrentSeason { get; set; }

        public LandingHomePage(ctrlDatabase Database)
        {
            this.Database = Database;

            InitializeComponent();

            createSeasonButton.Enabled = false;
            emulateFixtureButton.Enabled = false;

            //checks if user already answered the question
            if (!Database.CurrentUser.AnsweredFavouritClub && Database.ClubController.ClubList.Count() != 0)
            {
                
                    changeFavouritecClubButton_Click(null, null);
                Database.CurrentUser.AnsweredFavouritClub = true;
            }

            int count = 0;

            //checks how many active clubs there are
            foreach (tblClub club in Database.ClubController.ClubList)
            {
                if (club.Enabled)
                {
                    count++;
                }
            }

            //Checks if there are avaible clubs to start season
            if (Database.SeasonController.Seasons.Count() == 0 && Database.ClubController.ClubList.Count() >= 4 && count >= 4)
                {
                    DialogResult answer = MessageBox.Show("Nenhuma epoca encontrada, criar nova epoca?", "Epocas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (answer == DialogResult.Yes)
                    {
                        createSeasonButton_Click(null, null);
                    }
                }
            


            FormUpdate();
            
        }

        /// <summary>
        /// Updates the visual of form
        /// </summary>
        private void FormUpdate()
        {

            CurrentSeasonUpdate();

            UpdateFavouriteClub();

            UpdateAppControls();
            
        }

        /// <summary>
        /// Updates the visual part of Fixtures (The games made on that day)
        /// </summary>
        private void UpdateFixture()
        {
            currentFixtureGamesFlowLayout.Controls.Clear();
            if (CurrentSeason.Fixtures.Count() != 0)
            {
                

                foreach (tblMatch gamematch in CurrentSeason.Fixtures.Last().Matches)
                {
                    MatchUserControl uc = new MatchUserControl(gamematch);
                    uc.Size = new Size(510, uc.Size.Height);
                    currentFixtureGamesFlowLayout.Controls.Add(uc);
                }
            }
        }

        /// <summary>
        /// Updates the season information and classification board
        /// </summary>
        private void CurrentSeasonUpdate()
        {
            if (Database.SeasonController.Seasons.Count() != 0)
            {
                this.CurrentSeason = Database.SeasonController.Seasons.Last();
                Database.StatsControler.updateStatistics(CurrentSeason);

                if (CurrentSeason.Finished)
                {
                    foreach (Control control in this.Controls)
                    {
                        control.Visible = true;
                    }
                }

                //updates the classification board in order of position
                classificationPanel.Controls.Clear();
                int position = 0;
                while (classificationPanel.Controls.Count != CurrentSeason.Clubs.Count())
                {
                    position++;

                    foreach (tblClub club in CurrentSeason.Clubs) { 

                        if (position == club.Statistics.Last().Classification)
                        {
                            ClubClassificationTableUserControl uc = new ClubClassificationTableUserControl(CurrentSeason, club);
                            classificationPanel.Controls.Add(uc);
                        }
                    }
                }

                SeasonInformationUpdate();
                UpdateFixture();
            }

        }

        /// <summary>
        /// Updates the visual information of the season
        /// </summary>
        private void SeasonInformationUpdate()
        {
            CurrentSeasonNumberLabel.Text = CurrentSeason.ID.ToString();
            TotalSeasonFixturesLabel.Text = $"{CurrentSeason.Fixtures.Count().ToString()} / {CurrentSeason.MaxFixtures.ToString()} ";
            currentFixutreLabel.Text = CurrentSeason.Fixtures.Count().ToString() ;

            int GamesMatches = 0;

            foreach (tblFixtures fixture in CurrentSeason.Fixtures)
            {
                foreach (tblMatch match in fixture.Matches)
                {
                    GamesMatches++;
                }
            }
            TotalSeasonGamesLabel.Text = $"{GamesMatches.ToString()} / {CurrentSeason.MaxGamesMatches.ToString()}";

            if (CurrentSeason.Finished)
            {
                currentSeasonFinisehdLabel.Text = "Sim";
                createSeasonButton.Enabled = true;
                emulateFixtureButton.Enabled = false;
            }
            else
            {
                currentSeasonFinisehdLabel.Text = "Não";
                createSeasonButton.Enabled = false;
                emulateFixtureButton.Enabled = true;
            }

        }

        /// <summary>
        /// Updates the visual of the controls to control the app
        /// </summary>
        private void UpdateAppControls()
        {
            if (Database.SeasonController.Seasons.Count != 0)
            {
                if (Database.SeasonController.Seasons.Last().Finished)
                {
                    createSeasonButton.Enabled = true;
                }
                else
                {
                    createSeasonButton.Enabled = false;
                }

                foreach (Control control in this.Controls)
                {
                    control.Visible = true;
                }
            }
            else
            {
                createSeasonButton.Enabled = true;
            }
        }

        /// <summary>
        /// Updates the visual of the club the user has choosen
        /// </summary>
        private void UpdateFavouriteClub()
        {
            if (Database.CurrentUser.FavouriteClub != null)
            {
                clublogoPictureBox.BackColor = Color.Transparent;
                clublogoPictureBox.ImageLocation = Database.CurrentUser.FavouriteClub.LogoLocation;
                clubNameLabel.Text = Database.CurrentUser.FavouriteClub.Name;
                clubSeasonGroupbox.Visible = false;

                if (Database.SeasonController.Seasons.Count == 0)
                {
                    clubInSeasonLabel.Text = "Não";
                }
                else
                {
                    foreach (tblClub club in Database.SeasonController.Seasons.Last().Clubs)
                    {
                        if (club == Database.CurrentUser.FavouriteClub)
                        {
                            clubInSeasonLabel.Text = "Sim";

                            clubSeasonGroupbox.Visible = true;

                            ClubCurrentSeasonUpdate(Database.CurrentUser.FavouriteClub);

                            break;
                        }
                        else
                        {
                            clubInSeasonLabel.Text = "Não";
                        }
                    }
                }
            }
            else
            {
                clublogoPictureBox.BackColor = Color.Silver;
                clubNameLabel.Text = "-";
                clubSeasonGroupbox.Visible = false;
            }
        }

        /// <summary>
        /// Updates the visual information of the favourite club choosen
        /// </summary>
        /// <param name="favouriteClub">USer favourite club</param>
        private void ClubCurrentSeasonUpdate(tblClub favouriteClub)
        {
            clubCurrentPlaceLabel.Text = favouriteClub.Statistics.Last().Classification.ToString();
            clubCurrentPointsLabel.Text = favouriteClub.Statistics.Last().SeasonPoints.ToString();
            ClubCurrentVictoriesLabel.Text = favouriteClub.Statistics.Last().GamesWon.ToString();
        }

        private void changeFavouritecClubButton_Click(object sender, EventArgs e)
        {
            //checks if it possible to change club
            if (Database.ClubController.ClubList.Count() != 0)
            {
                DialogResult answer = MessageBox.Show("Escolher clube favourito?", "Clube Favourito", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    ChooseFavouriteClubForm frm = new ChooseFavouriteClubForm(Database);
                    frm.ShowDialog();
                    FormUpdate();
                }
            }
        }

        private void createSeasonButton_Click(object sender, EventArgs e)
        {
            int count = 0;

            //count the active clubs
            foreach(tblClub club in Database.ClubController.ClubList)
            {
                if (club.Enabled)
                {
                    count++;
                }
            }

            //checks if is possible to create a season
            if (Database.ClubController.ClubList.Count >= 4 && count >= 4)
            {
                Database.SeasonController.NewSeason(Database);
            }
            else
            {
                MessageBox.Show("Não existe clubes suficiente para começar uma epoca!");

            }

            FormUpdate();
        }

        private void emulateFixtureButton_Click(object sender, EventArgs e)
        {
            Database.FixtureController.NewFixture(Database,CurrentSeason);

            FormUpdate();

            Database.StatsControler.updateStatistics(CurrentSeason);
        }
    }
}
