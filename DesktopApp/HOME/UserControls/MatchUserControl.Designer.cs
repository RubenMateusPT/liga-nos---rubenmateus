﻿namespace DesktopApp
{
    partial class MatchUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.homeGoalsLabel = new System.Windows.Forms.Label();
            this.awayGoalLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.awayPictureBox = new System.Windows.Forms.PictureBox();
            this.homeClubPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.awayPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeClubPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // homeGoalsLabel
            // 
            this.homeGoalsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.homeGoalsLabel.AutoSize = true;
            this.homeGoalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeGoalsLabel.Location = new System.Drawing.Point(54, 9);
            this.homeGoalsLabel.Name = "homeGoalsLabel";
            this.homeGoalsLabel.Size = new System.Drawing.Size(32, 33);
            this.homeGoalsLabel.TabIndex = 2;
            this.homeGoalsLabel.Text = "5";
            // 
            // awayGoalLabel
            // 
            this.awayGoalLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.awayGoalLabel.AutoSize = true;
            this.awayGoalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.awayGoalLabel.Location = new System.Drawing.Point(152, 9);
            this.awayGoalLabel.Name = "awayGoalLabel";
            this.awayGoalLabel.Size = new System.Drawing.Size(32, 33);
            this.awayGoalLabel.TabIndex = 3;
            this.awayGoalLabel.Text = "5";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(91, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 33);
            this.label1.TabIndex = 4;
            this.label1.Text = "VS";
            // 
            // awayPictureBox
            // 
            this.awayPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.awayPictureBox.Location = new System.Drawing.Point(190, 3);
            this.awayPictureBox.Name = "awayPictureBox";
            this.awayPictureBox.Size = new System.Drawing.Size(45, 45);
            this.awayPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.awayPictureBox.TabIndex = 1;
            this.awayPictureBox.TabStop = false;
            // 
            // homeClubPictureBox
            // 
            this.homeClubPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.homeClubPictureBox.Location = new System.Drawing.Point(3, 3);
            this.homeClubPictureBox.Name = "homeClubPictureBox";
            this.homeClubPictureBox.Size = new System.Drawing.Size(45, 45);
            this.homeClubPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.homeClubPictureBox.TabIndex = 0;
            this.homeClubPictureBox.TabStop = false;
            // 
            // MatchUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.awayGoalLabel);
            this.Controls.Add(this.homeGoalsLabel);
            this.Controls.Add(this.awayPictureBox);
            this.Controls.Add(this.homeClubPictureBox);
            this.Name = "MatchUserControl";
            this.Size = new System.Drawing.Size(238, 55);
            ((System.ComponentModel.ISupportInitialize)(this.awayPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeClubPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox homeClubPictureBox;
        private System.Windows.Forms.PictureBox awayPictureBox;
        private System.Windows.Forms.Label homeGoalsLabel;
        private System.Windows.Forms.Label awayGoalLabel;
        private System.Windows.Forms.Label label1;
    }
}
