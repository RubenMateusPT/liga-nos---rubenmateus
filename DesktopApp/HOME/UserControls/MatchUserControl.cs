﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class MatchUserControl : UserControl
    { 
        private int ocThisWitdh { get; set; }

        public MatchUserControl(tblMatch gamematch)
        {
            

            InitializeComponent();

            ocThisWitdh = this.Width;


            homeClubPictureBox.ImageLocation = gamematch.HomeClub.LogoLocation;
            homeGoalsLabel.Text = gamematch.HomeGoals.ToString();

            awayPictureBox.ImageLocation = gamematch.AwayClub.LogoLocation;
            awayGoalLabel.Text = gamematch.AwayGoals.ToString();
        }

        /// <summary>
        /// Updates the location of label based on the size of user control
        /// </summary>
        public void UpdateLocation()
        {
            label1.Location = new Point(this.Width - ocThisWitdh);
        }
    }
}
