﻿namespace DesktopApp
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.paginaInicialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clubesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estatiscasDasEpocasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usercontrolPanel = new System.Windows.Forms.Panel();
            this.principalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDeDadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principalToolStripMenuItem,
            this.paginaInicialToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.toolStripMenuItem2,
            this.clubesToolStripMenuItem,
            this.estatiscasDasEpocasToolStripMenuItem,
            this.baseDeDadosToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // paginaInicialToolStripMenuItem
            // 
            this.paginaInicialToolStripMenuItem.Name = "paginaInicialToolStripMenuItem";
            this.paginaInicialToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.paginaInicialToolStripMenuItem.Text = "Pagina Inicial";
            this.paginaInicialToolStripMenuItem.Click += new System.EventHandler(this.paginaInicialToolStripMenuItem_Click);
            // 
            // clubesToolStripMenuItem
            // 
            this.clubesToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.clubesToolStripMenuItem.Name = "clubesToolStripMenuItem";
            this.clubesToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.clubesToolStripMenuItem.Text = "Clubes";
            this.clubesToolStripMenuItem.Click += new System.EventHandler(this.clubesToolStripMenuItem_Click);
            // 
            // estatiscasDasEpocasToolStripMenuItem
            // 
            this.estatiscasDasEpocasToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.estatiscasDasEpocasToolStripMenuItem.Name = "estatiscasDasEpocasToolStripMenuItem";
            this.estatiscasDasEpocasToolStripMenuItem.Size = new System.Drawing.Size(131, 20);
            this.estatiscasDasEpocasToolStripMenuItem.Text = "Estatiscas Das Epocas";
            this.estatiscasDasEpocasToolStripMenuItem.Click += new System.EventHandler(this.estatiscasDasEpocasToolStripMenuItem_Click);
            // 
            // usercontrolPanel
            // 
            this.usercontrolPanel.Location = new System.Drawing.Point(12, 27);
            this.usercontrolPanel.Name = "usercontrolPanel";
            this.usercontrolPanel.Size = new System.Drawing.Size(760, 522);
            this.usercontrolPanel.TabIndex = 1;
            // 
            // principalToolStripMenuItem
            // 
            this.principalToolStripMenuItem.Enabled = false;
            this.principalToolStripMenuItem.Name = "principalToolStripMenuItem";
            this.principalToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.principalToolStripMenuItem.Text = "Principal:";
            // 
            // baseDeDadosToolStripMenuItem
            // 
            this.baseDeDadosToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.baseDeDadosToolStripMenuItem.Enabled = false;
            this.baseDeDadosToolStripMenuItem.Name = "baseDeDadosToolStripMenuItem";
            this.baseDeDadosToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.baseDeDadosToolStripMenuItem.Text = "Base de Dados:";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem1.Enabled = false;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(22, 20);
            this.toolStripMenuItem1.Text = "|";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(22, 20);
            this.toolStripMenuItem2.Text = "|";
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.usercontrolPanel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainMenu";
            this.Text = "Liga NOS";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem paginaInicialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clubesToolStripMenuItem;
        private System.Windows.Forms.Panel usercontrolPanel;
        private System.Windows.Forms.ToolStripMenuItem estatiscasDasEpocasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem principalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem baseDeDadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}