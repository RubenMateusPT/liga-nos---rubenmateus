﻿using Library;
using System;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class MainMenu : Form
    {
        ctrlDatabase Database { get; set; }

        public MainMenu(ctrlDatabase Database,bool FirstTime)
        {
            this.Database = Database;

            InitializeComponent();

            paginaInicialToolStripMenuItem_Click(null, null);

        }


        private void clubesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates GUI to CLUBS
            usercontrolPanel.Controls.Clear();
            ClubsLandingPage uc = new ClubsLandingPage(Database);
            usercontrolPanel.Controls.Add(uc);

        }

        private void paginaInicialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates GUI to HomePage
            usercontrolPanel.Controls.Clear();
            LandingHomePage uc = new LandingHomePage(Database);
            usercontrolPanel.Controls.Add(uc);
        }

        private void estatiscasDasEpocasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates GUI to Statistics PAge
            usercontrolPanel.Controls.Clear();
            StatisticsLandingPageUserControl uc = new StatisticsLandingPageUserControl(Database,null);
            usercontrolPanel.Controls.Add(uc);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates form of about
            AboutForm frm = new AboutForm();
            frm.ShowDialog();
        }
    }
}
