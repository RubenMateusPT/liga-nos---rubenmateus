﻿namespace DesktopApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Library;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool firstTime = true;

            ctrlDatabase Database = new ctrlDatabase();
            

            Database.CreateDirectories();

            Database.LoadFile();

            if(Database.UserController.Users.Count != 0)
            {
                firstTime = true;
            }
            else
            {
                Database.UserController.NewUser(null);
            }

            Database.CurrentUser = Database.UserController.Users.ElementAt(0);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu(Database,firstTime));

            Database.SaveFile();
        }
    }
}
