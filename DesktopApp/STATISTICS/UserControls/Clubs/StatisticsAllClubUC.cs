﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsAllClubUC : UserControl
    {
        #region Atributes
        private int seasonPlayed = 0;
        private int fixturesPlayed = 0;
        private int matchPlayed = 0;

        private int totalWins = 0;
        private int totalWinsHome = 0;
        private int totalWinsAway = 0;

        private int totalDraws = 0;
        private int totalDrawsHome = 0;
        private int totalDrawsAway = 0;

        private int totalLosses = 0;
        private int totalLosesHome = 0;
        private int totalLosesAway = 0;

        private int totalScored = 0;
        private int totalScoredHome = 0;
        private int totalScoredAway = 0;

        private int totalConceded = 0;
        private int totalConcededHome = 0;
        private int totalConcededAway = 0;

        private int highestPosition = 100;
        private int hPSeasonId;

        private int highestPoints = 0;
        private int hPtsSeasonId = 0;

        private int lowestPostion = 0;
        private int lPSeasonId = 0;

        private int lowestPoints = 100;
        private int lPtsSeasonId = 0;
        #endregion


        private tblClub Club { get; set; }
        private ctrlDatabase Database { get; set; }

        public StatisticsAllClubUC(ctrlDatabase Database,tblClub Club)
        {
            this.Database = Database;
            this.Club = Club;

            InitializeComponent();

            clubNameLabel.Text = Club.Name;
            logo1Picturebox.ImageLocation = Club.LogoLocation;
            logo2Picturebox.ImageLocation = Club.LogoLocation;

            if (Club.Statistics.Count() != 0)
            {
                FindStats();

                FormUpdate();
            }

        }

        /// <summary>
        /// Updates the visual of the form
        /// </summary>
        private void FormUpdate()
        {
            seasonPlayedLabel.Text = seasonPlayed.ToString();
            fixturesPLayedLabel.Text = fixturesPlayed.ToString();
            gamesPlayedLabel.Text = matchPlayed.ToString();

            victoriesLabel.Text = totalWins.ToString();
            victorieshomeLabel.Text = totalWinsHome.ToString();
            victoriesawaylabel.Text = totalWinsAway.ToString();

            drawLabel.Text = totalDraws.ToString();
            drawhomeLabel.Text = totalDrawsHome.ToString();
            drawawayLabel.Text = totalDrawsAway.ToString();

            defeatLabel.Text = totalLosses.ToString();
            defeatHomeLabel.Text = totalLosesHome.ToString();
            defeatAwayLabel.Text = totalLosesAway.ToString();

            goalsScoredLabel.Text = totalScored.ToString();
            goalsScoredHomeLabel.Text = totalScoredHome.ToString();
            goalsScoredAwayLabel.Text = totalScoredAway.ToString();

            goalsConcededLabel.Text = totalConceded.ToString();
            goalsConcededHomeLabel.Text = totalConcededHome.ToString();
            goalsConcededAwayLabel.Text = totalConcededAway.ToString();

            highestClassificationLabel.Text = $"{highestPosition.ToString()} (Epoca {hPSeasonId.ToString()})";
            lowestClassificationLabel.Text = $"{lowestPostion.ToString()} (Epoca {lPSeasonId.ToString()})";

            highestPointsLabel.Text = $"{highestPoints.ToString()} (Epoca {hPtsSeasonId.ToString()})";
            lowestPointsLabel.Text = $"{lowestPoints.ToString()} (Epoca {lPtsSeasonId.ToString()})";

        }

        /// <summary>
        /// Finds the statistics of the club based on all season the club entered
        /// </summary>
        private void FindStats()
        {

            ///CAlculates the seasons,fixtures and gamematches played
            foreach(tblSeason season in Database.SeasonController.Seasons)
            {
                foreach(tblClub club in season.Clubs)
                {
                    if(Club == club)
                    {
                        seasonPlayed++;

                        foreach(tblFixtures fixture in season.Fixtures)
                        {
                            fixturesPlayed++;

                            foreach(tblMatch match in fixture.Matches)
                            {
                                if(match.HomeClub == club || match.AwayClub == club)
                                {
                                    matchPlayed++;
                                }
                            }
                        }
                    }
                }
            }


            foreach(tblStatistics stat in Club.Statistics)
            {

                totalWins += stat.GamesWon;
                totalWinsHome += stat.GamesWonHome;
                totalWinsAway += stat.GamesWonAway;

                totalDraws += stat.GamesTied;
                totalDrawsHome += stat.GamesTiedHome;
                totalDrawsAway += stat.GamesTiedAway;

                totalLosses += stat.GamesLost;
                totalLosesHome += stat.GamesLostHome;
                totalLosesAway += stat.GamesLostAway;

                totalScored += stat.GoalsScored;
                totalScoredHome += stat.GoalsScoredHome;
                totalScoredAway += stat.GoalsScoredAway;

                totalConceded += stat.GoalsConceded;
                totalConcededHome += stat.GoalsConcededHome;
                totalConcededAway += stat.GoalsConcededAway;

                if(highestPosition >= stat.Classification)
                {
                    highestPosition = stat.Classification;
                    hPSeasonId = stat.Season.ID;
                }
                if(lowestPostion <= stat.Classification)
                {
                    lowestPostion = stat.Classification;
                    lPSeasonId = stat.Season.ID;
                }

                if(highestPoints <= stat.SeasonPoints)
                {
                    highestPoints = stat.SeasonPoints;
                    hPtsSeasonId = stat.Season.ID;
                }

                if(lowestPoints >= stat.SeasonPoints)
                {
                    lowestPoints = stat.SeasonPoints;
                    lPtsSeasonId = stat.Season.ID;
                }

            }

        }
    }
}
