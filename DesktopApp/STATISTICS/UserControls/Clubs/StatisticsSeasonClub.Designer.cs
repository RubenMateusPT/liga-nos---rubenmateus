﻿namespace DesktopApp
{
    partial class StatisticsSeasonClub
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.seasonStatsGroupBox = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.goalsConcededAwayLabel = new System.Windows.Forms.Label();
            this.goalsConcededHomeLabel = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.goalsConcededLabel = new System.Windows.Forms.Label();
            this.goalsScoredAwayLabel = new System.Windows.Forms.Label();
            this.goalsScoredHomeLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.goalsScoredLabel = new System.Windows.Forms.Label();
            this.defeatAwayLabel = new System.Windows.Forms.Label();
            this.defeatHomeLabel = new System.Windows.Forms.Label();
            this.defeatLabel = new System.Windows.Forms.Label();
            this.drawawayLabel = new System.Windows.Forms.Label();
            this.drawhomeLabel = new System.Windows.Forms.Label();
            this.drawLabel = new System.Windows.Forms.Label();
            this.victoriesawaylabel = new System.Windows.Forms.Label();
            this.victorieshomeLabel = new System.Windows.Forms.Label();
            this.victoriesLabel = new System.Windows.Forms.Label();
            this.highestPointsLabel = new System.Windows.Forms.Label();
            this.highestClassificationLabel = new System.Windows.Forms.Label();
            this.gamesPlayedLabel = new System.Windows.Forms.Label();
            this.fixturesPLayedLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.seasonPlayedLabel = new System.Windows.Forms.Label();
            this.clubNameLabel = new System.Windows.Forms.Label();
            this.seasonStatsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // seasonStatsGroupBox
            // 
            this.seasonStatsGroupBox.Controls.Add(this.label15);
            this.seasonStatsGroupBox.Controls.Add(this.listBox1);
            this.seasonStatsGroupBox.Controls.Add(this.label22);
            this.seasonStatsGroupBox.Controls.Add(this.label31);
            this.seasonStatsGroupBox.Controls.Add(this.label30);
            this.seasonStatsGroupBox.Controls.Add(this.label29);
            this.seasonStatsGroupBox.Controls.Add(this.label28);
            this.seasonStatsGroupBox.Controls.Add(this.label27);
            this.seasonStatsGroupBox.Controls.Add(this.goalsConcededAwayLabel);
            this.seasonStatsGroupBox.Controls.Add(this.goalsConcededHomeLabel);
            this.seasonStatsGroupBox.Controls.Add(this.label23);
            this.seasonStatsGroupBox.Controls.Add(this.label24);
            this.seasonStatsGroupBox.Controls.Add(this.label25);
            this.seasonStatsGroupBox.Controls.Add(this.goalsConcededLabel);
            this.seasonStatsGroupBox.Controls.Add(this.goalsScoredAwayLabel);
            this.seasonStatsGroupBox.Controls.Add(this.goalsScoredHomeLabel);
            this.seasonStatsGroupBox.Controls.Add(this.label17);
            this.seasonStatsGroupBox.Controls.Add(this.label18);
            this.seasonStatsGroupBox.Controls.Add(this.label19);
            this.seasonStatsGroupBox.Controls.Add(this.goalsScoredLabel);
            this.seasonStatsGroupBox.Controls.Add(this.defeatAwayLabel);
            this.seasonStatsGroupBox.Controls.Add(this.defeatHomeLabel);
            this.seasonStatsGroupBox.Controls.Add(this.defeatLabel);
            this.seasonStatsGroupBox.Controls.Add(this.drawawayLabel);
            this.seasonStatsGroupBox.Controls.Add(this.drawhomeLabel);
            this.seasonStatsGroupBox.Controls.Add(this.drawLabel);
            this.seasonStatsGroupBox.Controls.Add(this.victoriesawaylabel);
            this.seasonStatsGroupBox.Controls.Add(this.victorieshomeLabel);
            this.seasonStatsGroupBox.Controls.Add(this.victoriesLabel);
            this.seasonStatsGroupBox.Controls.Add(this.highestPointsLabel);
            this.seasonStatsGroupBox.Controls.Add(this.highestClassificationLabel);
            this.seasonStatsGroupBox.Controls.Add(this.gamesPlayedLabel);
            this.seasonStatsGroupBox.Controls.Add(this.fixturesPLayedLabel);
            this.seasonStatsGroupBox.Controls.Add(this.label12);
            this.seasonStatsGroupBox.Controls.Add(this.label13);
            this.seasonStatsGroupBox.Controls.Add(this.label14);
            this.seasonStatsGroupBox.Controls.Add(this.label9);
            this.seasonStatsGroupBox.Controls.Add(this.label10);
            this.seasonStatsGroupBox.Controls.Add(this.label11);
            this.seasonStatsGroupBox.Controls.Add(this.label8);
            this.seasonStatsGroupBox.Controls.Add(this.label7);
            this.seasonStatsGroupBox.Controls.Add(this.label6);
            this.seasonStatsGroupBox.Controls.Add(this.label2);
            this.seasonStatsGroupBox.Controls.Add(this.label1);
            this.seasonStatsGroupBox.Controls.Add(this.label3);
            this.seasonStatsGroupBox.Controls.Add(this.label4);
            this.seasonStatsGroupBox.Controls.Add(this.label5);
            this.seasonStatsGroupBox.Controls.Add(this.seasonPlayedLabel);
            this.seasonStatsGroupBox.Location = new System.Drawing.Point(3, 53);
            this.seasonStatsGroupBox.Name = "seasonStatsGroupBox";
            this.seasonStatsGroupBox.Size = new System.Drawing.Size(514, 450);
            this.seasonStatsGroupBox.TabIndex = 46;
            this.seasonStatsGroupBox.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(286, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 78;
            this.label15.Text = "Clubes na Epoca:";
            // 
            // listBox1
            // 
            this.listBox1.Enabled = false;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(271, 160);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 277);
            this.listBox1.TabIndex = 77;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Location = new System.Drawing.Point(153, 12);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(1, 435);
            this.label22.TabIndex = 76;
            // 
            // label31
            // 
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label31.Location = new System.Drawing.Point(7, 376);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(139, 1);
            this.label31.TabIndex = 71;
            // 
            // label30
            // 
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Location = new System.Drawing.Point(8, 305);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(139, 1);
            this.label30.TabIndex = 70;
            // 
            // label29
            // 
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label29.Location = new System.Drawing.Point(7, 235);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(139, 1);
            this.label29.TabIndex = 69;
            // 
            // label28
            // 
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Location = new System.Drawing.Point(6, 159);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(139, 1);
            this.label28.TabIndex = 68;
            // 
            // label27
            // 
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label27.Location = new System.Drawing.Point(8, 81);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 1);
            this.label27.TabIndex = 67;
            // 
            // goalsConcededAwayLabel
            // 
            this.goalsConcededAwayLabel.AutoSize = true;
            this.goalsConcededAwayLabel.Location = new System.Drawing.Point(122, 215);
            this.goalsConcededAwayLabel.Name = "goalsConcededAwayLabel";
            this.goalsConcededAwayLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsConcededAwayLabel.TabIndex = 66;
            this.goalsConcededAwayLabel.Text = "100";
            this.goalsConcededAwayLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // goalsConcededHomeLabel
            // 
            this.goalsConcededHomeLabel.AutoSize = true;
            this.goalsConcededHomeLabel.Location = new System.Drawing.Point(122, 190);
            this.goalsConcededHomeLabel.Name = "goalsConcededHomeLabel";
            this.goalsConcededHomeLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsConcededHomeLabel.TabIndex = 65;
            this.goalsConcededHomeLabel.Text = "100";
            this.goalsConcededHomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(38, 166);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 64;
            this.label23.Tag = "";
            this.label23.Text = "Golos Sofridos:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 190);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 13);
            this.label24.TabIndex = 61;
            this.label24.Text = "Golos Sofridos Casa:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(14, 215);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(102, 13);
            this.label25.TabIndex = 62;
            this.label25.Text = "Golos Sofridos Fora:";
            // 
            // goalsConcededLabel
            // 
            this.goalsConcededLabel.AutoSize = true;
            this.goalsConcededLabel.Location = new System.Drawing.Point(122, 166);
            this.goalsConcededLabel.Name = "goalsConcededLabel";
            this.goalsConcededLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsConcededLabel.TabIndex = 63;
            this.goalsConcededLabel.Text = "100";
            this.goalsConcededLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // goalsScoredAwayLabel
            // 
            this.goalsScoredAwayLabel.AutoSize = true;
            this.goalsScoredAwayLabel.Location = new System.Drawing.Point(122, 138);
            this.goalsScoredAwayLabel.Name = "goalsScoredAwayLabel";
            this.goalsScoredAwayLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsScoredAwayLabel.TabIndex = 60;
            this.goalsScoredAwayLabel.Text = "100";
            this.goalsScoredAwayLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // goalsScoredHomeLabel
            // 
            this.goalsScoredHomeLabel.AutoSize = true;
            this.goalsScoredHomeLabel.Location = new System.Drawing.Point(122, 113);
            this.goalsScoredHomeLabel.Name = "goalsScoredHomeLabel";
            this.goalsScoredHomeLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsScoredHomeLabel.TabIndex = 59;
            this.goalsScoredHomeLabel.Text = "100";
            this.goalsScoredHomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(29, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 13);
            this.label17.TabIndex = 58;
            this.label17.Text = "Golos Marcados:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 13);
            this.label18.TabIndex = 55;
            this.label18.Text = "Golos Marcados Casa:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 138);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 13);
            this.label19.TabIndex = 56;
            this.label19.Text = "Golos Marcados Fora:";
            // 
            // goalsScoredLabel
            // 
            this.goalsScoredLabel.AutoSize = true;
            this.goalsScoredLabel.Location = new System.Drawing.Point(122, 89);
            this.goalsScoredLabel.Name = "goalsScoredLabel";
            this.goalsScoredLabel.Size = new System.Drawing.Size(25, 13);
            this.goalsScoredLabel.TabIndex = 57;
            this.goalsScoredLabel.Text = "100";
            this.goalsScoredLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // defeatAwayLabel
            // 
            this.defeatAwayLabel.AutoSize = true;
            this.defeatAwayLabel.Location = new System.Drawing.Point(122, 431);
            this.defeatAwayLabel.Name = "defeatAwayLabel";
            this.defeatAwayLabel.Size = new System.Drawing.Size(25, 13);
            this.defeatAwayLabel.TabIndex = 54;
            this.defeatAwayLabel.Text = "100";
            this.defeatAwayLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // defeatHomeLabel
            // 
            this.defeatHomeLabel.AutoSize = true;
            this.defeatHomeLabel.Location = new System.Drawing.Point(122, 407);
            this.defeatHomeLabel.Name = "defeatHomeLabel";
            this.defeatHomeLabel.Size = new System.Drawing.Size(25, 13);
            this.defeatHomeLabel.TabIndex = 53;
            this.defeatHomeLabel.Text = "100";
            this.defeatHomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // defeatLabel
            // 
            this.defeatLabel.AutoSize = true;
            this.defeatLabel.Location = new System.Drawing.Point(122, 383);
            this.defeatLabel.Name = "defeatLabel";
            this.defeatLabel.Size = new System.Drawing.Size(25, 13);
            this.defeatLabel.TabIndex = 52;
            this.defeatLabel.Text = "100";
            this.defeatLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // drawawayLabel
            // 
            this.drawawayLabel.AutoSize = true;
            this.drawawayLabel.Location = new System.Drawing.Point(122, 357);
            this.drawawayLabel.Name = "drawawayLabel";
            this.drawawayLabel.Size = new System.Drawing.Size(25, 13);
            this.drawawayLabel.TabIndex = 51;
            this.drawawayLabel.Text = "100";
            this.drawawayLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // drawhomeLabel
            // 
            this.drawhomeLabel.AutoSize = true;
            this.drawhomeLabel.Location = new System.Drawing.Point(122, 334);
            this.drawhomeLabel.Name = "drawhomeLabel";
            this.drawhomeLabel.Size = new System.Drawing.Size(25, 13);
            this.drawhomeLabel.TabIndex = 50;
            this.drawhomeLabel.Text = "100";
            this.drawhomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // drawLabel
            // 
            this.drawLabel.AutoSize = true;
            this.drawLabel.Location = new System.Drawing.Point(122, 312);
            this.drawLabel.Name = "drawLabel";
            this.drawLabel.Size = new System.Drawing.Size(25, 13);
            this.drawLabel.TabIndex = 49;
            this.drawLabel.Text = "100";
            this.drawLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // victoriesawaylabel
            // 
            this.victoriesawaylabel.AutoSize = true;
            this.victoriesawaylabel.Location = new System.Drawing.Point(122, 286);
            this.victoriesawaylabel.Name = "victoriesawaylabel";
            this.victoriesawaylabel.Size = new System.Drawing.Size(25, 13);
            this.victoriesawaylabel.TabIndex = 48;
            this.victoriesawaylabel.Text = "100";
            this.victoriesawaylabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // victorieshomeLabel
            // 
            this.victorieshomeLabel.AutoSize = true;
            this.victorieshomeLabel.Location = new System.Drawing.Point(122, 264);
            this.victorieshomeLabel.Name = "victorieshomeLabel";
            this.victorieshomeLabel.Size = new System.Drawing.Size(25, 13);
            this.victorieshomeLabel.TabIndex = 47;
            this.victorieshomeLabel.Text = "100";
            this.victorieshomeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // victoriesLabel
            // 
            this.victoriesLabel.AutoSize = true;
            this.victoriesLabel.Location = new System.Drawing.Point(122, 242);
            this.victoriesLabel.Name = "victoriesLabel";
            this.victoriesLabel.Size = new System.Drawing.Size(25, 13);
            this.victoriesLabel.TabIndex = 46;
            this.victoriesLabel.Text = "100";
            this.victoriesLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // highestPointsLabel
            // 
            this.highestPointsLabel.Location = new System.Drawing.Point(372, 109);
            this.highestPointsLabel.Name = "highestPointsLabel";
            this.highestPointsLabel.Size = new System.Drawing.Size(118, 13);
            this.highestPointsLabel.TabIndex = 45;
            this.highestPointsLabel.Text = "100";
            this.highestPointsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // highestClassificationLabel
            // 
            this.highestClassificationLabel.Location = new System.Drawing.Point(183, 109);
            this.highestClassificationLabel.Name = "highestClassificationLabel";
            this.highestClassificationLabel.Size = new System.Drawing.Size(125, 13);
            this.highestClassificationLabel.TabIndex = 44;
            this.highestClassificationLabel.Text = "100";
            this.highestClassificationLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gamesPlayedLabel
            // 
            this.gamesPlayedLabel.AutoSize = true;
            this.gamesPlayedLabel.Location = new System.Drawing.Point(122, 61);
            this.gamesPlayedLabel.Name = "gamesPlayedLabel";
            this.gamesPlayedLabel.Size = new System.Drawing.Size(25, 13);
            this.gamesPlayedLabel.TabIndex = 43;
            this.gamesPlayedLabel.Text = "100";
            this.gamesPlayedLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fixturesPLayedLabel
            // 
            this.fixturesPLayedLabel.AutoSize = true;
            this.fixturesPLayedLabel.Location = new System.Drawing.Point(122, 36);
            this.fixturesPLayedLabel.Name = "fixturesPLayedLabel";
            this.fixturesPLayedLabel.Size = new System.Drawing.Size(25, 13);
            this.fixturesPLayedLabel.TabIndex = 42;
            this.fixturesPLayedLabel.Text = "100";
            this.fixturesPLayedLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(40, 431);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Derrotas fora:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 407);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Derrotas em Casa:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(61, 383);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Derrotas:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 357);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Empates fora:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Empates em Casa:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(65, 312);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Empates:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(51, 286);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Vitorias fora:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Vitorias em casa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(72, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Vitorias:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(387, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Pontuação Feita:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Epocas nº:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Jornadas Jogadas:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Jogos Jogados:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Classificação:";
            // 
            // seasonPlayedLabel
            // 
            this.seasonPlayedLabel.AutoSize = true;
            this.seasonPlayedLabel.Location = new System.Drawing.Point(122, 12);
            this.seasonPlayedLabel.Name = "seasonPlayedLabel";
            this.seasonPlayedLabel.Size = new System.Drawing.Size(25, 13);
            this.seasonPlayedLabel.TabIndex = 30;
            this.seasonPlayedLabel.Text = "100";
            this.seasonPlayedLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // clubNameLabel
            // 
            this.clubNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clubNameLabel.Location = new System.Drawing.Point(12, 8);
            this.clubNameLabel.Name = "clubNameLabel";
            this.clubNameLabel.Size = new System.Drawing.Size(505, 42);
            this.clubNameLabel.TabIndex = 45;
            this.clubNameLabel.Text = "Liga NOS";
            this.clubNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StatisticsSeasonClub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.seasonStatsGroupBox);
            this.Controls.Add(this.clubNameLabel);
            this.Name = "StatisticsSeasonClub";
            this.Size = new System.Drawing.Size(520, 510);
            this.seasonStatsGroupBox.ResumeLayout(false);
            this.seasonStatsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox seasonStatsGroupBox;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label goalsConcededAwayLabel;
        private System.Windows.Forms.Label goalsConcededHomeLabel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label goalsConcededLabel;
        private System.Windows.Forms.Label goalsScoredAwayLabel;
        private System.Windows.Forms.Label goalsScoredHomeLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label goalsScoredLabel;
        private System.Windows.Forms.Label defeatAwayLabel;
        private System.Windows.Forms.Label defeatHomeLabel;
        private System.Windows.Forms.Label defeatLabel;
        private System.Windows.Forms.Label drawawayLabel;
        private System.Windows.Forms.Label drawhomeLabel;
        private System.Windows.Forms.Label drawLabel;
        private System.Windows.Forms.Label victoriesawaylabel;
        private System.Windows.Forms.Label victorieshomeLabel;
        private System.Windows.Forms.Label victoriesLabel;
        private System.Windows.Forms.Label highestPointsLabel;
        private System.Windows.Forms.Label highestClassificationLabel;
        private System.Windows.Forms.Label gamesPlayedLabel;
        private System.Windows.Forms.Label fixturesPLayedLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label seasonPlayedLabel;
        private System.Windows.Forms.Label clubNameLabel;
        private System.Windows.Forms.Label label15;
    }
}
