﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{ 

    public partial class StatisticsSeasonClub : UserControl
    {
        #region Atributes

        
        private int seasonPlayed = 0;
        private int fixturesPlayed = 0;
        private int matchPlayed = 0;

        private int totalWins = 0;
        private int totalWinsHome = 0;
        private int totalWinsAway = 0;

        private int totalDraws = 0;
        private int totalDrawsHome = 0;
        private int totalDrawsAway = 0;

        private int totalLosses = 0;
        private int totalLosesHome = 0;
        private int totalLosesAway = 0;

        private int totalScored = 0;
        private int totalScoredHome = 0;
        private int totalScoredAway = 0;

        private int totalConceded = 0;
        private int totalConcededHome = 0;
        private int totalConcededAway = 0;

        private int Position = 100;

        private int Points = 0;
        #endregion


        private tblSeason Season { get; set; }
        private tblClub Club { get; set; }


        public StatisticsSeasonClub(tblSeason Season, tblClub Club)
        {
            this.Season = Season;
            this.Club = Club;

            InitializeComponent();

            
            //Shows or hides if latest season has not finishied
            if (Club.Statistics.Count() != 0)
            {
                if (Season.Finished)
                {
                    clubNameLabel.Text = $"Epoca nº {Season.ID}";

                    seasonStatsGroupBox.Visible = true;

                    FindStats();

                    FormUpdate();
                }
                else 
                {
                    clubNameLabel.Text = $"Epoca nº {Season.ID} (A decorrer)";

                    seasonStatsGroupBox.Visible = false;
                }

                
            }
            
        }

        /// <summary>
        /// Finds all Statitiscs of Current Club in Current Season
        /// </summary>
        private void FindStats()
        {
            

            foreach(tblStatistics stat in Club.Statistics)
            {
                if (stat.Season == Season)
                {
                    fixturesPlayed = stat.GamesPlayed;
                    matchPlayed = stat.GamesPlayed;


                    totalWins = stat.GamesWon;
                    totalWinsHome = stat.GamesWonHome;
                    totalWinsAway = stat.GamesWonAway;

                    totalDraws = stat.GamesTied;
                    totalDrawsHome = stat.GamesTiedHome;
                    totalDrawsAway = stat.GamesTiedAway;

                    totalLosses = stat.GamesLost;
                    totalLosesHome = stat.GamesLostHome;
                    totalLosesAway = stat.GamesLostAway;

                    totalScored = stat.GoalsScored;
                    totalScoredHome = stat.GoalsScoredHome;
                    totalScoredAway = stat.GoalsScoredAway;

                    totalConceded = stat.GoalsConceded;
                    totalConcededHome = stat.GoalsConcededHome;
                    totalConcededAway = stat.GoalsConcededAway;

                    Position = stat.Classification;

                    Points = stat.SeasonPoints;
                }

            }



        }

        /// <summary>
        /// Updates the visual of the form
        /// </summary>
        private void FormUpdate()
        {
            seasonPlayedLabel.Text = seasonPlayed.ToString();
            fixturesPLayedLabel.Text = fixturesPlayed.ToString();
            gamesPlayedLabel.Text = matchPlayed.ToString();

            victoriesLabel.Text = totalWins.ToString();
            victorieshomeLabel.Text = totalWinsHome.ToString();
            victoriesawaylabel.Text = totalWinsAway.ToString();

            drawLabel.Text = totalDraws.ToString();
            drawhomeLabel.Text = totalDrawsHome.ToString();
            drawawayLabel.Text = totalDrawsAway.ToString();

            defeatLabel.Text = totalLosses.ToString();
            defeatHomeLabel.Text = totalLosesHome.ToString();
            defeatAwayLabel.Text = totalLosesAway.ToString();

            goalsScoredLabel.Text = totalScored.ToString();
            goalsScoredHomeLabel.Text = totalScoredHome.ToString();
            goalsScoredAwayLabel.Text = totalScoredAway.ToString();

            goalsConcededLabel.Text = totalConceded.ToString();
            goalsConcededHomeLabel.Text = totalConcededHome.ToString();
            goalsConcededAwayLabel.Text = totalConcededAway.ToString();

            highestClassificationLabel.Text = $"{Position.ToString()}";

            highestPointsLabel.Text = $"{Points.ToString()}";

            listBox1.Items.Clear();
            foreach (tblClub club in Season.Clubs)
            {
                listBox1.Items.Add(club);
                listBox1.DisplayMember = "Name";
            }

        }
    }
}
