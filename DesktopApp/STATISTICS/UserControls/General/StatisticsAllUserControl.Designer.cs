﻿namespace DesktopApp
{
    partial class StatisticsAllUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.totalSeasonLabel = new System.Windows.Forms.Label();
            this.fixturesTotalLabel = new System.Windows.Forms.Label();
            this.gamematchesTotalLAbel = new System.Windows.Forms.Label();
            this.totalscoredGoalsLabel = new System.Windows.Forms.Label();
            this.mostseasonVictoriesPicturebox = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mostLossesPicturebox = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.mostGoalsConcededPicturebox = new System.Windows.Forms.PictureBox();
            this.mostGoalsscoredPicturebox = new System.Windows.Forms.PictureBox();
            this.mostVicotoriesSeasonsNameLabel = new System.Windows.Forms.Label();
            this.mostLossesLabel = new System.Windows.Forms.Label();
            this.mostGoalsScoredLabel = new System.Windows.Forms.Label();
            this.mostGoalsConcededLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mostseasonVictoriesPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostLossesPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsConcededPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsscoredPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(157, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Liga NOS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Epocas Totais:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(139, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jornadas Totais:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Jogos Totais:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(393, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Total Golos Marcados:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(328, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Maior Derrotas:";
            // 
            // totalSeasonLabel
            // 
            this.totalSeasonLabel.Location = new System.Drawing.Point(17, 98);
            this.totalSeasonLabel.Name = "totalSeasonLabel";
            this.totalSeasonLabel.Size = new System.Drawing.Size(78, 23);
            this.totalSeasonLabel.TabIndex = 6;
            this.totalSeasonLabel.Text = "100";
            this.totalSeasonLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fixturesTotalLabel
            // 
            this.fixturesTotalLabel.Location = new System.Drawing.Point(141, 98);
            this.fixturesTotalLabel.Name = "fixturesTotalLabel";
            this.fixturesTotalLabel.Size = new System.Drawing.Size(78, 23);
            this.fixturesTotalLabel.TabIndex = 7;
            this.fixturesTotalLabel.Text = "100";
            this.fixturesTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gamematchesTotalLAbel
            // 
            this.gamematchesTotalLAbel.Location = new System.Drawing.Point(269, 98);
            this.gamematchesTotalLAbel.Name = "gamematchesTotalLAbel";
            this.gamematchesTotalLAbel.Size = new System.Drawing.Size(78, 23);
            this.gamematchesTotalLAbel.TabIndex = 8;
            this.gamematchesTotalLAbel.Text = "100";
            this.gamematchesTotalLAbel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // totalscoredGoalsLabel
            // 
            this.totalscoredGoalsLabel.Location = new System.Drawing.Point(411, 98);
            this.totalscoredGoalsLabel.Name = "totalscoredGoalsLabel";
            this.totalscoredGoalsLabel.Size = new System.Drawing.Size(78, 23);
            this.totalscoredGoalsLabel.TabIndex = 9;
            this.totalscoredGoalsLabel.Text = "100";
            this.totalscoredGoalsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mostseasonVictoriesPicturebox
            // 
            this.mostseasonVictoriesPicturebox.Location = new System.Drawing.Point(88, 169);
            this.mostseasonVictoriesPicturebox.Name = "mostseasonVictoriesPicturebox";
            this.mostseasonVictoriesPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostseasonVictoriesPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostseasonVictoriesPicturebox.TabIndex = 10;
            this.mostseasonVictoriesPicturebox.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(91, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Mais Vitorias:";
            // 
            // mostLossesPicturebox
            // 
            this.mostLossesPicturebox.Location = new System.Drawing.Point(329, 169);
            this.mostLossesPicturebox.Name = "mostLossesPicturebox";
            this.mostLossesPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostLossesPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostLossesPicturebox.TabIndex = 12;
            this.mostLossesPicturebox.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 321);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Mais Golos Marcados:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(317, 321);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Mais Golos Sofridos:";
            // 
            // mostGoalsConcededPicturebox
            // 
            this.mostGoalsConcededPicturebox.Location = new System.Drawing.Point(329, 337);
            this.mostGoalsConcededPicturebox.Name = "mostGoalsConcededPicturebox";
            this.mostGoalsConcededPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostGoalsConcededPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostGoalsConcededPicturebox.TabIndex = 16;
            this.mostGoalsConcededPicturebox.TabStop = false;
            // 
            // mostGoalsscoredPicturebox
            // 
            this.mostGoalsscoredPicturebox.Location = new System.Drawing.Point(88, 337);
            this.mostGoalsscoredPicturebox.Name = "mostGoalsscoredPicturebox";
            this.mostGoalsscoredPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostGoalsscoredPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostGoalsscoredPicturebox.TabIndex = 15;
            this.mostGoalsscoredPicturebox.TabStop = false;
            // 
            // mostVicotoriesSeasonsNameLabel
            // 
            this.mostVicotoriesSeasonsNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostVicotoriesSeasonsNameLabel.Location = new System.Drawing.Point(71, 256);
            this.mostVicotoriesSeasonsNameLabel.Name = "mostVicotoriesSeasonsNameLabel";
            this.mostVicotoriesSeasonsNameLabel.Size = new System.Drawing.Size(108, 13);
            this.mostVicotoriesSeasonsNameLabel.TabIndex = 17;
            this.mostVicotoriesSeasonsNameLabel.Text = "Sporting";
            this.mostVicotoriesSeasonsNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mostLossesLabel
            // 
            this.mostLossesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostLossesLabel.Location = new System.Drawing.Point(312, 256);
            this.mostLossesLabel.Name = "mostLossesLabel";
            this.mostLossesLabel.Size = new System.Drawing.Size(108, 13);
            this.mostLossesLabel.TabIndex = 18;
            this.mostLossesLabel.Text = "Sporting";
            this.mostLossesLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mostGoalsScoredLabel
            // 
            this.mostGoalsScoredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostGoalsScoredLabel.Location = new System.Drawing.Point(71, 426);
            this.mostGoalsScoredLabel.Name = "mostGoalsScoredLabel";
            this.mostGoalsScoredLabel.Size = new System.Drawing.Size(108, 13);
            this.mostGoalsScoredLabel.TabIndex = 19;
            this.mostGoalsScoredLabel.Text = "Sporting";
            this.mostGoalsScoredLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mostGoalsConcededLabel
            // 
            this.mostGoalsConcededLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostGoalsConcededLabel.Location = new System.Drawing.Point(312, 426);
            this.mostGoalsConcededLabel.Name = "mostGoalsConcededLabel";
            this.mostGoalsConcededLabel.Size = new System.Drawing.Size(108, 13);
            this.mostGoalsConcededLabel.TabIndex = 20;
            this.mostGoalsConcededLabel.Text = "Sporting";
            this.mostGoalsConcededLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StatisticsAllUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mostGoalsConcededLabel);
            this.Controls.Add(this.mostGoalsScoredLabel);
            this.Controls.Add(this.mostLossesLabel);
            this.Controls.Add(this.mostVicotoriesSeasonsNameLabel);
            this.Controls.Add(this.mostGoalsConcededPicturebox);
            this.Controls.Add(this.mostGoalsscoredPicturebox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.mostLossesPicturebox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.mostseasonVictoriesPicturebox);
            this.Controls.Add(this.totalscoredGoalsLabel);
            this.Controls.Add(this.gamematchesTotalLAbel);
            this.Controls.Add(this.fixturesTotalLabel);
            this.Controls.Add(this.totalSeasonLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "StatisticsAllUserControl";
            this.Size = new System.Drawing.Size(520, 510);
            ((System.ComponentModel.ISupportInitialize)(this.mostseasonVictoriesPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostLossesPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsConcededPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsscoredPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label totalSeasonLabel;
        private System.Windows.Forms.Label fixturesTotalLabel;
        private System.Windows.Forms.Label gamematchesTotalLAbel;
        private System.Windows.Forms.Label totalscoredGoalsLabel;
        private System.Windows.Forms.PictureBox mostseasonVictoriesPicturebox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox mostLossesPicturebox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox mostGoalsConcededPicturebox;
        private System.Windows.Forms.PictureBox mostGoalsscoredPicturebox;
        private System.Windows.Forms.Label mostVicotoriesSeasonsNameLabel;
        private System.Windows.Forms.Label mostLossesLabel;
        private System.Windows.Forms.Label mostGoalsScoredLabel;
        private System.Windows.Forms.Label mostGoalsConcededLabel;
    }
}
