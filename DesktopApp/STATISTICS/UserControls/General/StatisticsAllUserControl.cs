﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsAllUserControl : UserControl
    {
        #region Atributes
        private int totalSeason = 0;
        private int totalFixtures = 0;
        private int totalGames = 0;
        private int totalGoals = 0;

        private tblClub MostWinsClub;
        private int temptotalclubwins = 0;
        private int totalclubwins = 0;
        private tblClub MostLossClub;
        private int temptotalclublosses = 0;
        private int totalclublosses = 0;
        private tblClub MostGoalsScoredClub;
        private int temptotalclubgoasscored = 0;
        private int totalclubgoalsscored = 0;
        private tblClub MostGoalConcededClub;
        private int temptotalclubgoalsconceded = 0;
        private int totalclubgoalsconceded = 0;

        #endregion

        private List<tblSeason> SeasonList { get; set; }

        public StatisticsAllUserControl(ctrlDatabase Database,List<tblSeason> SeasonList)
        {
            this.SeasonList = SeasonList;
            InitializeComponent();

            FormUpdate(Database);
        }

        /// <summary>
        /// Update visual of the form
        /// </summary>
        /// <param name="database">Current DAtabase</param>
        private void FormUpdate(ctrlDatabase database)
        {

            FindStats(database);

            totalSeasonLabel.Text = totalSeason.ToString();
            fixturesTotalLabel.Text = totalFixtures.ToString();
            gamematchesTotalLAbel.Text = totalGames.ToString();
            totalscoredGoalsLabel.Text = totalGoals.ToString();

            mostseasonVictoriesPicturebox.ImageLocation = MostWinsClub.LogoLocation;
            mostVicotoriesSeasonsNameLabel.Text = MostWinsClub.Name;

            mostLossesPicturebox.ImageLocation = MostLossClub.LogoLocation;
            mostLossesLabel.Text = MostLossClub.Name;

            mostGoalsscoredPicturebox.ImageLocation = MostGoalsScoredClub.LogoLocation;
            mostGoalsScoredLabel.Text = MostGoalsScoredClub.Name;

            mostGoalsConcededPicturebox.ImageLocation = MostGoalConcededClub.LogoLocation;
            mostGoalsConcededLabel.Text = MostGoalConcededClub.Name;

        }

        /// <summary>
        /// Finds the Statistics of All Season
        /// </summary>
        /// <param name="database">Current database</param>
        private void FindStats(ctrlDatabase database)
        {
            foreach (tblSeason season in SeasonList)
            {
                totalSeason++;

                foreach (tblFixtures fixture in season.Fixtures)
                {
                    totalFixtures++;

                    foreach (tblMatch match in fixture.Matches)
                    {
                        totalGames++;

                        totalGoals += (match.AwayGoals + match.HomeGoals);
                    }
                }
            }

            foreach (tblClub club in database.ClubController.ClubList)
            {
                temptotalclubgoalsconceded = 0;
                temptotalclubgoasscored = 0;
                temptotalclublosses = 0;
                temptotalclubwins = 0;

                foreach (tblStatistics stats in club.Statistics)
                {
                    if (stats.Classification == 1)
                    {
                        temptotalclubwins++;
                    }

                    temptotalclubgoasscored += stats.GoalsScored;

                    temptotalclubgoalsconceded += stats.GoalsConceded;

                    temptotalclublosses += stats.GamesLost;
                }

                if (temptotalclubwins > totalclubwins)
                {
                    totalclubwins = temptotalclubwins;
                    MostWinsClub = club;
                }
                if (temptotalclubgoasscored > totalclubgoalsscored)
                {
                    totalclubgoalsscored = temptotalclubgoasscored;
                    MostGoalsScoredClub = club;
                }
                if (temptotalclublosses > totalclublosses)
                {
                    totalclublosses = temptotalclublosses;
                    MostLossClub = club;
                }
                if (temptotalclubgoalsconceded > totalclubgoalsconceded)
                {
                    totalclubgoalsconceded = temptotalclublosses;
                    MostGoalConcededClub = club;
                }
            }

        }
    }
}
