﻿namespace DesktopApp
{
    partial class StatisticsFixtureUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.seasonStatsGroupBox = new System.Windows.Forms.GroupBox();
            this.mostGoalsConcededLabel = new System.Windows.Forms.Label();
            this.mostGoalsScoredLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mostGoalsConcededPicturebox = new System.Windows.Forms.PictureBox();
            this.mostGoalsscoredPicturebox = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gamematchesTotalLAbel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.totalscoredGoalsLabel = new System.Windows.Forms.Label();
            this.fixtureNumberLabel = new System.Windows.Forms.Label();
            this.seasonStatsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsConcededPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsscoredPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // seasonStatsGroupBox
            // 
            this.seasonStatsGroupBox.Controls.Add(this.mostGoalsConcededLabel);
            this.seasonStatsGroupBox.Controls.Add(this.mostGoalsScoredLabel);
            this.seasonStatsGroupBox.Controls.Add(this.label4);
            this.seasonStatsGroupBox.Controls.Add(this.label5);
            this.seasonStatsGroupBox.Controls.Add(this.mostGoalsConcededPicturebox);
            this.seasonStatsGroupBox.Controls.Add(this.mostGoalsscoredPicturebox);
            this.seasonStatsGroupBox.Controls.Add(this.label9);
            this.seasonStatsGroupBox.Controls.Add(this.gamematchesTotalLAbel);
            this.seasonStatsGroupBox.Controls.Add(this.label8);
            this.seasonStatsGroupBox.Controls.Add(this.totalscoredGoalsLabel);
            this.seasonStatsGroupBox.Location = new System.Drawing.Point(3, 48);
            this.seasonStatsGroupBox.Name = "seasonStatsGroupBox";
            this.seasonStatsGroupBox.Size = new System.Drawing.Size(514, 455);
            this.seasonStatsGroupBox.TabIndex = 44;
            this.seasonStatsGroupBox.TabStop = false;
            // 
            // mostGoalsConcededLabel
            // 
            this.mostGoalsConcededLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostGoalsConcededLabel.Location = new System.Drawing.Point(299, 253);
            this.mostGoalsConcededLabel.Name = "mostGoalsConcededLabel";
            this.mostGoalsConcededLabel.Size = new System.Drawing.Size(108, 13);
            this.mostGoalsConcededLabel.TabIndex = 41;
            this.mostGoalsConcededLabel.Text = "Sporting";
            this.mostGoalsConcededLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mostGoalsScoredLabel
            // 
            this.mostGoalsScoredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostGoalsScoredLabel.Location = new System.Drawing.Point(109, 253);
            this.mostGoalsScoredLabel.Name = "mostGoalsScoredLabel";
            this.mostGoalsScoredLabel.Size = new System.Drawing.Size(108, 13);
            this.mostGoalsScoredLabel.TabIndex = 40;
            this.mostGoalsScoredLabel.Text = "Sporting";
            this.mostGoalsScoredLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Jogos Totais:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(290, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Total Golos Marcados:";
            // 
            // mostGoalsConcededPicturebox
            // 
            this.mostGoalsConcededPicturebox.Location = new System.Drawing.Point(316, 164);
            this.mostGoalsConcededPicturebox.Name = "mostGoalsConcededPicturebox";
            this.mostGoalsConcededPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostGoalsConcededPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostGoalsConcededPicturebox.TabIndex = 37;
            this.mostGoalsConcededPicturebox.TabStop = false;
            // 
            // mostGoalsscoredPicturebox
            // 
            this.mostGoalsscoredPicturebox.Location = new System.Drawing.Point(126, 164);
            this.mostGoalsscoredPicturebox.Name = "mostGoalsscoredPicturebox";
            this.mostGoalsscoredPicturebox.Size = new System.Drawing.Size(75, 75);
            this.mostGoalsscoredPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mostGoalsscoredPicturebox.TabIndex = 36;
            this.mostGoalsscoredPicturebox.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(304, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "Mais Golos Sofridos:";
            // 
            // gamematchesTotalLAbel
            // 
            this.gamematchesTotalLAbel.Location = new System.Drawing.Point(123, 52);
            this.gamematchesTotalLAbel.Name = "gamematchesTotalLAbel";
            this.gamematchesTotalLAbel.Size = new System.Drawing.Size(78, 23);
            this.gamematchesTotalLAbel.TabIndex = 29;
            this.gamematchesTotalLAbel.Text = "100";
            this.gamematchesTotalLAbel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(109, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Mais Golos Marcados:";
            // 
            // totalscoredGoalsLabel
            // 
            this.totalscoredGoalsLabel.Location = new System.Drawing.Point(308, 52);
            this.totalscoredGoalsLabel.Name = "totalscoredGoalsLabel";
            this.totalscoredGoalsLabel.Size = new System.Drawing.Size(78, 23);
            this.totalscoredGoalsLabel.TabIndex = 30;
            this.totalscoredGoalsLabel.Text = "100";
            this.totalscoredGoalsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fixtureNumberLabel
            // 
            this.fixtureNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixtureNumberLabel.Location = new System.Drawing.Point(12, 8);
            this.fixtureNumberLabel.Name = "fixtureNumberLabel";
            this.fixtureNumberLabel.Size = new System.Drawing.Size(505, 42);
            this.fixtureNumberLabel.TabIndex = 43;
            this.fixtureNumberLabel.Text = "Liga NOS";
            this.fixtureNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StatisticsFixtureUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.seasonStatsGroupBox);
            this.Controls.Add(this.fixtureNumberLabel);
            this.Name = "StatisticsFixtureUC";
            this.Size = new System.Drawing.Size(520, 510);
            this.seasonStatsGroupBox.ResumeLayout(false);
            this.seasonStatsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsConcededPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostGoalsscoredPicturebox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox seasonStatsGroupBox;
        private System.Windows.Forms.Label mostGoalsConcededLabel;
        private System.Windows.Forms.Label mostGoalsScoredLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox mostGoalsConcededPicturebox;
        private System.Windows.Forms.PictureBox mostGoalsscoredPicturebox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label gamematchesTotalLAbel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label totalscoredGoalsLabel;
        private System.Windows.Forms.Label fixtureNumberLabel;
    }
}
