﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsFixtureUC : UserControl
    {
        private int totalGames = 0;
        private int totalGoals = 0;

        
        private tblClub MostGoalsScoredClub;
        private tblClub MostGoalConcededClub;

        public StatisticsFixtureUC(tblFixtures Fixture)
        {
            InitializeComponent();

                fixtureNumberLabel.Text = $"Jornada nº {Fixture.ID}";
            FindStats(Fixture);
                FormUpdate(Fixture);
        }

        /// <summary>
        /// Updates the visual of form
        /// </summary>
        /// <param name="Fixture"></param>
        private void FormUpdate(tblFixtures Fixture)
        {

            FindStats(Fixture);

            gamematchesTotalLAbel.Text = totalGames.ToString();
            totalscoredGoalsLabel.Text = totalGoals.ToString();

            mostGoalsscoredPicturebox.ImageLocation = MostGoalsScoredClub.LogoLocation;
            mostGoalsScoredLabel.Text = MostGoalsScoredClub.Name;

            mostGoalsConcededPicturebox.ImageLocation = MostGoalConcededClub.LogoLocation;
            mostGoalsConcededLabel.Text = MostGoalConcededClub.Name;

        }

        /// <summary>
        /// Finds the status of the fixture
        /// </summary>
        /// <param name="Fixture">Current fixture to work with</param>
        private void FindStats(tblFixtures Fixture)
        {

            int tempMostGoals = -1;
            int tempMostConceded = 99;

            do
            {
                foreach (tblMatch match in Fixture.Matches)
                {
                    totalGames++;

                    totalGoals += (match.AwayGoals + match.HomeGoals);

                    if (match.HomeGoals > match.AwayGoals)
                    {
                        if (tempMostGoals < match.HomeGoals)
                        {
                            MostGoalsScoredClub = match.HomeClub;
                            tempMostGoals = match.HomeGoals;
                        }

                        if(tempMostConceded > match.HomeGoals)
                        {
                            MostGoalConcededClub = match.AwayClub;
                            tempMostConceded = match.HomeGoals;
                        }

                    }
                    else
                    {
                        if (tempMostGoals < match.AwayGoals)
                        {
                            MostGoalsScoredClub = match.AwayClub;
                            tempMostGoals = match.AwayGoals;  
                        }

                        if (tempMostConceded > match.AwayGoals)
                        {
                            MostGoalConcededClub = match.HomeClub;
                            tempMostConceded = match.AwayGoals;
                        }

                    }
                }
            } while (MostGoalsScoredClub == null || MostGoalConcededClub == null);
        }
    }
}
