﻿namespace DesktopApp
{
    partial class StatisticsMatchUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fixtureNumberLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fixtureNumberLabel
            // 
            this.fixtureNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixtureNumberLabel.Location = new System.Drawing.Point(3, 12);
            this.fixtureNumberLabel.Name = "fixtureNumberLabel";
            this.fixtureNumberLabel.Size = new System.Drawing.Size(505, 42);
            this.fixtureNumberLabel.TabIndex = 44;
            this.fixtureNumberLabel.Text = "Liga NOS";
            this.fixtureNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StatisticsMatchUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fixtureNumberLabel);
            this.Name = "StatisticsMatchUC";
            this.Size = new System.Drawing.Size(520, 510);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label fixtureNumberLabel;
    }
}
