﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsMatchUC : UserControl
    {
        public StatisticsMatchUC(tblMatch match)
        {
            InitializeComponent();

            fixtureNumberLabel.Text = $"Partida nº {match.ID}";

            MatchUserControl uc = new MatchUserControl(match);
            uc.Size = new Size(this.Width, uc.Height);
            uc.Location = new Point(0,this.Height/2);
            this.Controls.Add(uc);
        }

    }
}
