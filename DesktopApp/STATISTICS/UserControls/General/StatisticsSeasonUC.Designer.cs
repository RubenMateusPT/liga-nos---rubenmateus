﻿namespace DesktopApp
{
    partial class StatisticsSeasonUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalscoredGoalsLabel = new System.Windows.Forms.Label();
            this.gamematchesTotalLAbel = new System.Windows.Forms.Label();
            this.fixturesTotalLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.seasonNumberLabel = new System.Windows.Forms.Label();
            this.seasonStatsGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.classificationPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.seasonStatsGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // totalscoredGoalsLabel
            // 
            this.totalscoredGoalsLabel.Location = new System.Drawing.Point(368, 51);
            this.totalscoredGoalsLabel.Name = "totalscoredGoalsLabel";
            this.totalscoredGoalsLabel.Size = new System.Drawing.Size(78, 23);
            this.totalscoredGoalsLabel.TabIndex = 30;
            this.totalscoredGoalsLabel.Text = "100";
            this.totalscoredGoalsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gamematchesTotalLAbel
            // 
            this.gamematchesTotalLAbel.Location = new System.Drawing.Point(220, 51);
            this.gamematchesTotalLAbel.Name = "gamematchesTotalLAbel";
            this.gamematchesTotalLAbel.Size = new System.Drawing.Size(78, 23);
            this.gamematchesTotalLAbel.TabIndex = 29;
            this.gamematchesTotalLAbel.Text = "100";
            this.gamematchesTotalLAbel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fixturesTotalLabel
            // 
            this.fixturesTotalLabel.Location = new System.Drawing.Point(64, 51);
            this.fixturesTotalLabel.Name = "fixturesTotalLabel";
            this.fixturesTotalLabel.Size = new System.Drawing.Size(78, 23);
            this.fixturesTotalLabel.TabIndex = 28;
            this.fixturesTotalLabel.Text = "100";
            this.fixturesTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(350, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Total Golos Marcados:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Jogos Totais:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Jornadas Totais:";
            // 
            // seasonNumberLabel
            // 
            this.seasonNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seasonNumberLabel.Location = new System.Drawing.Point(12, 12);
            this.seasonNumberLabel.Name = "seasonNumberLabel";
            this.seasonNumberLabel.Size = new System.Drawing.Size(505, 42);
            this.seasonNumberLabel.TabIndex = 21;
            this.seasonNumberLabel.Text = "Liga NOS";
            this.seasonNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // seasonStatsGroupBox
            // 
            this.seasonStatsGroupBox.Controls.Add(this.groupBox3);
            this.seasonStatsGroupBox.Controls.Add(this.classificationPanel);
            this.seasonStatsGroupBox.Controls.Add(this.label3);
            this.seasonStatsGroupBox.Controls.Add(this.label4);
            this.seasonStatsGroupBox.Controls.Add(this.label5);
            this.seasonStatsGroupBox.Controls.Add(this.fixturesTotalLabel);
            this.seasonStatsGroupBox.Controls.Add(this.gamematchesTotalLAbel);
            this.seasonStatsGroupBox.Controls.Add(this.totalscoredGoalsLabel);
            this.seasonStatsGroupBox.Location = new System.Drawing.Point(3, 52);
            this.seasonStatsGroupBox.Name = "seasonStatsGroupBox";
            this.seasonStatsGroupBox.Size = new System.Drawing.Size(514, 455);
            this.seasonStatsGroupBox.TabIndex = 42;
            this.seasonStatsGroupBox.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Location = new System.Drawing.Point(7, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(501, 40);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(470, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Pts";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "GS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(385, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "GM";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(349, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "D";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(306, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "E";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "V";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(219, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "JJ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(50, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Equipa";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "POS";
            // 
            // classificationPanel
            // 
            this.classificationPanel.AutoScroll = true;
            this.classificationPanel.Location = new System.Drawing.Point(6, 123);
            this.classificationPanel.Name = "classificationPanel";
            this.classificationPanel.Size = new System.Drawing.Size(501, 326);
            this.classificationPanel.TabIndex = 31;
            // 
            // StatisticsSeasonUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.seasonStatsGroupBox);
            this.Controls.Add(this.seasonNumberLabel);
            this.Name = "StatisticsSeasonUC";
            this.Size = new System.Drawing.Size(520, 510);
            this.seasonStatsGroupBox.ResumeLayout(false);
            this.seasonStatsGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label totalscoredGoalsLabel;
        private System.Windows.Forms.Label gamematchesTotalLAbel;
        private System.Windows.Forms.Label fixturesTotalLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label seasonNumberLabel;
        private System.Windows.Forms.GroupBox seasonStatsGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel classificationPanel;
    }
}
