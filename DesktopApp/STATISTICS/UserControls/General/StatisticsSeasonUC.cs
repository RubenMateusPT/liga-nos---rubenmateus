﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsSeasonUC : UserControl
    {
        private int totalFixtures = 0;
        private int totalGames = 0;
        private int totalGoals = 0;

       

        public StatisticsSeasonUC(tblSeason Season)
        {
            InitializeComponent();

            if (Season.Finished)
            {
                seasonStatsGroupBox.Visible = true;
                seasonNumberLabel.Text = $"Epoca nº {Season.ID}";
                FormUpdate(Season);
            }
            else{
                seasonStatsGroupBox.Visible = false;
                seasonNumberLabel.Text = $"Epoca nº {Season.ID} (A decorrer)";
            }

            
        }

        /// <summary>
        /// Updated the visual of the form
        /// </summary>
        /// <param name="Season"></param>
        private void FormUpdate(tblSeason Season)
        {

            FindStats(Season);

            fixturesTotalLabel.Text = totalFixtures.ToString();
            gamematchesTotalLAbel.Text = totalGames.ToString();
            totalscoredGoalsLabel.Text = totalGoals.ToString();

            MakeClassificationTable(Season);

        }

        /// <summary>
        /// Creates the classification board with the correct order
        /// </summary>
        /// <param name="Season">Current Season</param>
        private void MakeClassificationTable(tblSeason Season)
        {
            classificationPanel.Controls.Clear();

            int position = 0;

            while (classificationPanel.Controls.Count != Season.Clubs.Count())
            {
                position++;

                foreach (tblClub club in Season.Clubs)
                {
                    foreach (tblStatistics stat in club.Statistics)
                    {
                        if (stat.Season == Season && position == stat.Classification)
                        {
                            ClubClassificationTableUserControl uc = new ClubClassificationTableUserControl(Season, club);
                            uc.Size = new Size(classificationPanel.Width, uc.Height);
                            classificationPanel.Controls.Add(uc);
                        }
                    }
                }
            }


        }

        /// <summary>
        /// Finds the statistis of current season
        /// </summary>
        /// <param name="Season">Current season</param>
        private void FindStats(tblSeason Season)
        {

                foreach (tblFixtures fixture in Season.Fixtures)
                {
                    totalFixtures++;

                    foreach (tblMatch match in fixture.Matches)
                    {
                        totalGames++;

                        totalGoals += (match.AwayGoals + match.HomeGoals);
                    }
                }


        }
    }
}
