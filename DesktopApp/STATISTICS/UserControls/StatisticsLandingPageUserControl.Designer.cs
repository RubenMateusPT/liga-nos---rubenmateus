﻿namespace DesktopApp
{
    partial class StatisticsLandingPageUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.seasonTreeView = new System.Windows.Forms.TreeView();
            this.informationPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // seasonTreeView
            // 
            this.seasonTreeView.Location = new System.Drawing.Point(3, 3);
            this.seasonTreeView.Name = "seasonTreeView";
            this.seasonTreeView.Size = new System.Drawing.Size(207, 514);
            this.seasonTreeView.TabIndex = 0;
            this.seasonTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.seasonTreeView_AfterSelect);
            // 
            // informationPanel
            // 
            this.informationPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.informationPanel.Location = new System.Drawing.Point(216, 3);
            this.informationPanel.Name = "informationPanel";
            this.informationPanel.Size = new System.Drawing.Size(541, 514);
            this.informationPanel.TabIndex = 1;
            // 
            // StatisticsLandingPageUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.informationPanel);
            this.Controls.Add(this.seasonTreeView);
            this.Name = "StatisticsLandingPageUserControl";
            this.Size = new System.Drawing.Size(760, 520);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView seasonTreeView;
        private System.Windows.Forms.Panel informationPanel;
    }
}
