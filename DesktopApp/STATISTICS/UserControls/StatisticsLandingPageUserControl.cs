﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace DesktopApp
{
    public partial class StatisticsLandingPageUserControl : UserControl
    {
        private ctrlDatabase Database { get; set; }
        private tblClub Club { get; set; }

        /// <summary>
        /// Visual Base for Statistics
        /// </summary>
        /// <param name="Database">Working database</param>
        /// <param name="Club">Club if accesed from club page (can be null)</param>
        public StatisticsLandingPageUserControl(ctrlDatabase Database,tblClub Club)
        {
            this.Database = Database;
            this.Club = Club;

            InitializeComponent();

            FormUpdate();
        }

        /// <summary>
        /// Updates Visual and all controls of Form
        /// </summary>
        private void FormUpdate()
        {
            seasonTreeView.Nodes.Clear();

            //"All" information of season
            TreeNode database = new TreeNode("Geral");
            database.Tag = Database.SeasonController.Seasons;
            seasonTreeView.Nodes.Add(database);

            //Populates tree with Season nodes based if it was accessded by the homepage or the club page
            if (Club != null)
            {
                foreach (tblStatistics stats in Club.Statistics.OrderByDescending(stats => stats.Season.ID))
                {
                    foreach (tblSeason season in Database.SeasonController.Seasons.OrderByDescending(season => season.ID))
                    {
                        if (season.ID == stats.Season.ID)
                        {
                            PopulateTreeSeason(season);
                        }
                    }
                }
            }
            else
            {
                foreach (tblSeason season in Database.SeasonController.Seasons.OrderByDescending(season => season.ID))
                {

                    PopulateTreeSeason(season);
                }
            }

            
        }

        /// <summary>
        /// Populates Season nodes with Fixture nodes
        /// </summary>
        /// <param name="season">Season to work with</param>
        private void PopulateTreeSeason(tblSeason season)
        {
            TreeNode seasonNode = new TreeNode($"Epoca nº: {season.ID.ToString()}");
            seasonNode.Tag = season;

            if (season == Database.SeasonController.Seasons.Last() && !season.Finished)
            {
                seasonNode.Text += " (A decorrer)";
            }

            seasonTreeView.Nodes.Add(seasonNode);

            foreach (tblFixtures fixture in season.Fixtures.OrderByDescending(fixture => fixture.ID))
            {
                PopulateTreeFixture(seasonNode,fixture);
            }
        }

        /// <summary>
        /// Pupulates Season nodes with Fixture
        /// </summary>
        /// <param name="seasonNode">Current SeasonNode</param>
        /// <param name="fixture">Current fixture</param>
        private void PopulateTreeFixture(TreeNode seasonNode, tblFixtures fixture)
        {
            TreeNode fixtureNode = new TreeNode($"Jornada nº: {fixture.ID.ToString()}");
            fixtureNode.Tag = fixture;

            seasonNode.Nodes.Add(fixtureNode);

            foreach (tblMatch match in fixture.Matches.OrderByDescending(match => match.ID))
            {
                if(Club != null)
                {
                    if (Club == match.HomeClub || Club == match.AwayClub)
                    {
                        PopupateTreeMatch(fixtureNode, match);
                    }
                }
                else
                {
                    PopupateTreeMatch(fixtureNode, match);
                }
            }
        }

        /// <summary>
        /// Populates Fxiture nodes wit GameMatchNodes
        /// </summary>
        /// <param name="fixtureNode">Current FixtureNode</param>
        /// <param name="match">Gamematch from fixture</param>
        private void PopupateTreeMatch(TreeNode fixtureNode, tblMatch match)
        {
            TreeNode matchNode = new TreeNode($"Partida nº: {match.ID.ToString()}");
            matchNode.Tag = match;
            fixtureNode.Nodes.Add(matchNode);
        }

        /// <summary>
        /// Event after selecting a tree node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void seasonTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (seasonTreeView.SelectedNode.Tag)
            {
                //If node is "All"
                case List<tblSeason> SeasonsList:
                    {
                        informationPanel.Controls.Clear();

                        if (Club == null)
                        {
                            StatisticsAllUserControl uc;

                            uc = new StatisticsAllUserControl(Database, SeasonsList);

                            informationPanel.Controls.Add(uc);
                        }
                        else
                        {
                            StatisticsAllClubUC uc;

                            uc = new StatisticsAllClubUC(Database, Club);
                            uc.Location = new Point(uc.Location.X+10,uc.Location.Y);

                            informationPanel.Controls.Add(uc);
                        }

                        

                    } break;

                    //if nodes is a season
                case tblSeason Season:
                    {
                        informationPanel.Controls.Clear();

                        if (Club == null)
                        {
                            StatisticsSeasonUC uc;

                            uc = new StatisticsSeasonUC(Season);

                            informationPanel.Controls.Add(uc);
                        }
                        else
                        {
                            StatisticsSeasonClub uc;

                            uc = new StatisticsSeasonClub(Season, Club);
                            uc.Location = new Point(uc.Location.X + 10, uc.Location.Y);

                            informationPanel.Controls.Add(uc);
                        }
                    }
                    break;

                    //if node is a fixture
                case tblFixtures Fixture:
                    {
                        informationPanel.Controls.Clear();

                        if (Club == null)
                        {
                            StatisticsFixtureUC uc;

                            uc = new StatisticsFixtureUC(Fixture);

                            informationPanel.Controls.Add(uc);
                        }
                    }
                    break;

                    //if node is async Gamematch
                case tblMatch Match:
                    {
                        informationPanel.Controls.Clear();


                            StatisticsMatchUC uc;

                            uc = new StatisticsMatchUC(Match);

                            informationPanel.Controls.Add(uc);
                        

                    }
                    break;
            }
        }
    }
}
