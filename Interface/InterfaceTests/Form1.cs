﻿using System;
using System.IO;
using System.Windows.Forms;
using ControllerClasses;
using ModelClasses;


namespace InterfaceTests
{
    public partial class Form1 : Form
    {
        ClubController Club;
        MemberController Member;
        GameMatchController GameMatch;


        public Form1()
        {
            InitializeComponent();
            Club = new ClubController();
            Member = new MemberController();
            GameMatch = new GameMatchController();

            MyUpdate();
            UpdateMember();
        }

        #region CreateClub

        
        private void createBt_Click(object sender, System.EventArgs e)
        {
            Club.NewClub(textBox2.Text, textBox3.Text, textBox4.Text, Club.BackupLogo(textBox2.Text,pictureBox1.ImageLocation));
            MyUpdate();
            UpdateMember();
        }

        private void MyUpdate()
        {
            int newID = 0;
            foreach (ClubModel club in Club.ClubList)
            {
                if (club.ID > newID)
                {
                    newID = club.ID;
                }
            }

            textBox1.Text = (newID+1).ToString();

            listBox1.DataSource = null;
            listBox1.DataSource = Club.ClubList;
            listBox1.DisplayMember = "ClubInformation";
            listBox1.ValueMember = "ID";

            
        }


        private void deleteButton_Click(object sender, System.EventArgs e)
        {
            Club.RemoveClub((ClubModel)listBox1.SelectedItem);
            MyUpdate();
        }

        private void pictureBox1_Click(object sender, System.EventArgs e)
        {
            openFileDialog1.ShowDialog();
            pictureBox1.ImageLocation = openFileDialog1.FileName;
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ClubModel clubToEdit = (ClubModel)listBox1.SelectedItem;
            textBox1.Text = clubToEdit.ID.ToString();
            textBox2.Text = clubToEdit.Name;
            textBox3.Text = clubToEdit.Description;
            textBox4.Text = clubToEdit.StadiumName;
            textBox5.Text = clubToEdit.LogoLocation;
            pictureBox1.ImageLocation = clubToEdit.LogoLocation;

            listBox3.DataSource = null;
            listBox3.DataSource = clubToEdit.StaffList;
            listBox3.DisplayMember = "Name";

            listBox4.DataSource = null;
            listBox4.DataSource = clubToEdit.GameMatchHome;
            listBox4.DisplayMember = "ClubInformation";

            listBox5.DataSource = null;
            listBox5.DataSource = clubToEdit.GameMatchAway;
            listBox5.DisplayMember = "ClubInformation";

            label6.Text = clubToEdit.Name;


        }

        private void editButton_Click(object sender, System.EventArgs e)
        {
            ClubModel clubToEdit = (ClubModel)listBox1.SelectedItem;
            Club.EditClub(clubToEdit, textBox2.Text, textBox3.Text, textBox4.Text, Club.BackupLogo(textBox2.Text, pictureBox1.ImageLocation));
            MyUpdate();
        }
        #endregion

        #region MemberControll
        private void UpdateMember()
        {
            int newID = 0;
            foreach (MemberModel member in Member.MemberList)
            {
                if (member.ID > newID)
                {
                    newID = member.ID;
                }
            }

            idOc.Text = (newID + 1).ToString();

            listBox2.DataSource = null;
            listBox2.DataSource = Member.MemberList;
            listBox2.DisplayMember = "MemberInformation";
            listBox2.ValueMember = "ID";

            clubCombo.DataSource = null;
            clubCombo.DataSource = Club.ClubList;
            clubCombo.DisplayMember = "Name";

            comboBox1.DataSource = null;
            comboBox1.DataSource = Club.ClubList;
            comboBox1.DisplayMember = "Name";

            comboBox2.DataSource = null;
            comboBox2.DataSource = Club.ClubList;
            comboBox2.DisplayMember = "Name";


            ocOcCbx.Items.Clear();
            foreach(string value  in Enum.GetNames(typeof(Ocupation)))
            {
                ocOcCbx.Items.Add(value);
            }
            
        }

        private void createOC_Click(object sender, System.EventArgs e)
        {
            Member.NewMember((ClubModel)clubCombo.SelectedItem, nomeOc.Text, (Ocupation) ocOcCbx.SelectedIndex, Convert.ToInt32(numericUpDown1.Value));
            UpdateMember();

        }

        private void delOC_Click(object sender, EventArgs e)
        {
            Member.RemoveMember((MemberModel)listBox2.SelectedItem);
            UpdateMember();
        }


        private void editOC_Click(object sender, EventArgs e)
        {
            Member.EditMember((MemberModel)listBox2.SelectedItem, (ClubModel)clubCombo.SelectedItem, nomeOc.Text, (Ocupation)ocOcCbx.SelectedIndex, Convert.ToInt32(numericUpDown1.Value));
            UpdateMember();
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MemberModel memberToEdit = (MemberModel)listBox2.SelectedItem;
            idOc.Text = memberToEdit.ID.ToString();
            clubCombo.Text = memberToEdit.Club.Name;
            nomeOc.Text = memberToEdit.Name;
            ocOcCbx.ValueMember = memberToEdit.Ocupation.ToString();
            numericUpDown1.Value = memberToEdit.ShirtNumber;

        }


        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            string tableCub = @"ClubInfo.txt";
            string tableJogadores = @"JogadoresInfo.txt";
            string tableJogos = @"JogosInfo.txt";

            StreamReader sr;

            if (File.Exists(tableCub))
            {
                sr = File.OpenText(tableCub);

                string linha = string.Empty;

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = linha.Split('|');

                    Club.LoadFromFile(int.Parse(campos[0]), campos[1], campos[2], campos[3], campos[4]);
                }

                sr.Close();
            }

            if (File.Exists(tableJogadores))
            {
                sr = File.OpenText(tableJogadores);

                string linha = string.Empty;

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = linha.Split('|');

                    ClubModel clubToAdd = null;

                    foreach(ClubModel club in Club.ClubList)
                    {
                        if(club.ID == int.Parse(campos[0]))
                        {
                            clubToAdd = club;
                        }
                    }

                    Member.LoadFromFile(clubToAdd, int.Parse(campos[1]), campos[2], (Ocupation) Enum.Parse(typeof(Ocupation),campos[3]), int.Parse(campos[4]));
                }

                sr.Close();
            }

            if (File.Exists(tableJogos))
            {
                sr = File.OpenText(tableJogos);

                string linha = string.Empty;

                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = linha.Split('|');

                    ClubModel homeClub = null;
                    ClubModel awayClub = null;

                    foreach (ClubModel club in Club.ClubList)
                    {
                        if (club.ID == int.Parse(campos[0]))
                        {
                            homeClub = club;
                        }
                        if(club.ID == int.Parse(campos[3]))
                        {
                            awayClub = club;
                        }
                    }

                    GameMatch.LoadFromFile(homeClub, int.Parse(campos[1]), bool.Parse(campos[2]), awayClub);
                }

                sr.Close();
            }


            MyUpdate();
            UpdateMember();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            string tableClub = @"ClubInfo.txt";
            string tableJogadores = @"JogadoresInfo.txt";
            string tableJogos = @"JogosInfo.txt";

            StreamWriter swClub = new StreamWriter(tableClub, false);
            StreamWriter swJogadores = new StreamWriter(tableJogadores, false);
            StreamWriter swJogos = new StreamWriter(tableJogos, false);

            if (!File.Exists(tableClub))
            {
                swClub = File.CreateText(tableClub);
            }
            if (!File.Exists(tableJogadores))
            {
                swJogadores =File.CreateText(tableJogadores) ;
            }
            if (!File.Exists(tableJogos))
            {
                swJogos = File.CreateText(tableJogos);
            }

            foreach (ClubModel club in Club.ClubList)
            {
                swClub.WriteLine($"{club.ID}|{club.Name}|{club.Description}|{club.StadiumName}|{club.LogoLocation}");

                foreach (MemberModel member in club.StaffList)
                {
                    swJogadores.WriteLine($"{member.Club.ID}|{member.ID}|{member.Name}|{member.Ocupation}|{member.ShirtNumber}");
                }

                foreach(GameMatchModel homeGame in club.GameMatchHome)
                {
                    swJogos.WriteLine($"{homeGame.MyClub.ID}|{homeGame.GoalsScored}|{homeGame.PlayedHome}|{homeGame.RivalClub.ID}");
                }

                foreach (GameMatchModel homeGame in club.GameMatchAway)
                {
                    swJogos.WriteLine($"{homeGame.MyClub.ID}|{homeGame.GoalsScored}|{homeGame.PlayedHome}|{homeGame.RivalClub.ID}");
                }
            }


            swClub.Close();
            swJogadores.Close();
            swJogos.Close();

        }

        private void comboBox1_ValueMemberChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox2_ValueMemberChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == comboBox2.SelectedItem)
            {
                comboBox1.SelectedItem = null;
                MessageBox.Show("O clube nao pode jogar contra si mesmo");
            }
        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem == comboBox1.SelectedItem)
            {
                comboBox2.SelectedItem = null;
                MessageBox.Show("O clube nao pode jogar contra si mesmo");
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GameMatch.NewGame((ClubModel)comboBox1.SelectedItem, Convert.ToInt32(numericUpDown2.Value), true,(ClubModel) comboBox2.SelectedItem);
            GameMatch.NewGame((ClubModel)comboBox2.SelectedItem, Convert.ToInt32(numericUpDown3.Value), false, (ClubModel) comboBox1.SelectedItem);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GameMatch.EditGame((GameMatchModel)listBox4.SelectedItem, (ClubModel)listBox1.SelectedItem, Convert.ToInt32(numericUpDown2.Value), true, (ClubModel)comboBox2.SelectedItem);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            GameMatch.RemoveGame((GameMatchModel)listBox4.SelectedItem, (ClubModel)listBox1.SelectedItem);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            GameMatch.EditGame((GameMatchModel)listBox5.SelectedItem, (ClubModel)listBox1.SelectedItem, Convert.ToInt32(numericUpDown2.Value), false, (ClubModel)comboBox2.SelectedItem);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GameMatch.RemoveGame((GameMatchModel)listBox5.SelectedItem, (ClubModel)listBox1.SelectedItem);
        }


    }
}
