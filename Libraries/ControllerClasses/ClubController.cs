﻿namespace ControllerClasses
{
    using ModelClasses;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ClubController 
    {

        public List<ClubModel> ClubList { get; private set; }

        public ClubController()
        {
            ClubList = new List<ClubModel>();
        }

        public void NewClub(string Name,string Description, string StadiumName, string LogoLocation )
        {
            ClubModel newClub = new ClubModel();

            int newID = 0;
            foreach (ClubModel club in ClubList)
            {
                if (club.ID > newID)
                {
                    newID = club.ID;
                }
            }

            newClub.ID = newID+1;
            newClub.Name = Name;
            newClub.Description = Description;
            newClub.StadiumName = StadiumName;
            newClub.LogoLocation = LogoLocation;
            newClub.StaffList = new List<MemberModel>();
            newClub.GameMatchHome = new List<GameMatchModel>();
            newClub.GameMatchAway = new List<GameMatchModel>();

            ClubList.Add(newClub);

        }

        public void EditClub(ClubModel clubToEdit, string Name, string Description, string StadiumName, string LogoLocation)
        {

            if (!clubToEdit.Name.Equals(Name))
            {
                File.Delete(clubToEdit.LogoLocation);
            }

            clubToEdit.Name = Name;
            clubToEdit.Description = Description;
            clubToEdit.StadiumName = StadiumName;
            clubToEdit.LogoLocation = LogoLocation;
        }

        public void RemoveClub (ClubModel clubToRemove)
        {

            File.Delete(clubToRemove.LogoLocation);

            ClubList.Remove(clubToRemove);
        }

        public string BackupLogo(string Name, string logoLocation)
        {
            string logoDestination = $@"..\..\..\..\Resources\ClubLogos\{Name.ToUpper()}.png";

            try
            {
                File.Copy(logoLocation, logoDestination, true);
            }
            catch (Exception ex)
            {
                //User canceled operation
            }

            return logoDestination;

        }


        public void LoadFromFile(int ID, string Name, string Description, string StadiumName, string LogoLocation)
        {
            ClubModel loadedClub = new ClubModel();

            loadedClub.ID = ID;
            loadedClub.Name = Name;
            loadedClub.Description = Description;
            loadedClub.StadiumName = StadiumName;
            loadedClub.LogoLocation = LogoLocation;
            loadedClub.StaffList = new List<MemberModel>();
            loadedClub.GameMatchHome = new List<GameMatchModel>();
            loadedClub.GameMatchAway = new List<GameMatchModel>();

            ClubList.Add(loadedClub);

        }
    }
}
