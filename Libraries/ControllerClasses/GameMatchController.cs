﻿using ModelClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerClasses
{
    public class GameMatchController
    {

        public void NewGame(ClubModel MyClub, int GoalScored, bool Home,ClubModel RivalClub )
        {
            GameMatchModel newMatch = new GameMatchModel();

            newMatch.MyClub = MyClub;
            newMatch.GoalsScored = GoalScored;
            newMatch.PlayedHome = Home;
            newMatch.RivalClub = RivalClub;

            if (Home)
            {
                MyClub.GameMatchHome.Add(newMatch);
            }
            else
            {
                MyClub.GameMatchAway.Add(newMatch);
            }
        }

        public void EditGame(GameMatchModel GameToEdit,ClubModel MyClub, int GoalScored, bool Home, ClubModel RivalClub)
        {

            if (GameToEdit.PlayedHome)
            {
                MyClub.GameMatchHome.Remove(GameToEdit);
            }
            else
            {
                MyClub.GameMatchAway.Remove(GameToEdit);
            }

            GameToEdit.MyClub = MyClub;
            GameToEdit.GoalsScored = GoalScored;
            GameToEdit.PlayedHome = Home;
            GameToEdit.RivalClub = RivalClub;

            if (Home)
            {
                MyClub.GameMatchHome.Add(GameToEdit);
            }
            else
            {
                MyClub.GameMatchAway.Add(GameToEdit);
            }

        }

        public void RemoveGame(GameMatchModel GameToEdit, ClubModel MyClub)
        {

            if (GameToEdit.PlayedHome)
            {
                MyClub.GameMatchHome.Remove(GameToEdit);
                
            }
            else
            {
                MyClub.GameMatchAway.Remove(GameToEdit);
            }

        }

        public void LoadFromFile(ClubModel MyClub, int GoalScored, bool Home, ClubModel RivalClub)
        {
            GameMatchModel newMatch = new GameMatchModel();

            newMatch.MyClub = MyClub;
            newMatch.GoalsScored = GoalScored;
            newMatch.PlayedHome = Home;
            newMatch.RivalClub = RivalClub;

            if (Home)
            {
                MyClub.GameMatchHome.Add(newMatch);
            }
            else
            {
                MyClub.GameMatchAway.Add(newMatch);
            }
        }
    }
}
