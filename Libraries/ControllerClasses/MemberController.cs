﻿namespace ControllerClasses
{
    using ModelClasses;
    using System.Collections.Generic;

    public class MemberController
    {
        public List<MemberModel> MemberList { get; private set; }

        public MemberController()
        {
            MemberList = new List<MemberModel>();
        }


        public void NewMember(ClubModel Club,string Name, Ocupation Ocupation,int ShirtNumber)
        {
            MemberModel newMember = new MemberModel();

            int newID = 0;
            foreach (MemberModel member in MemberList)
            {
                if (member.ID > newID)
                {
                    newID = member.ID;
                }
            }

            newMember.ID = newID + 1;
            newMember.Club = Club;
            newMember.Name = Name;
            newMember.Ocupation = Ocupation;
            newMember.ShirtNumber = ShirtNumber;

            MemberList.Add(newMember);
            Club.StaffList.Add(newMember);
        }

        public void EditMember(MemberModel memberToEdit,ClubModel club, string Name, Ocupation Ocupation, int ShirtNumber)
        {

            memberToEdit.Club.StaffList.Remove(memberToEdit);
            club.StaffList.Add(memberToEdit);
            
            memberToEdit.Club = club;
            memberToEdit.Name = Name;
            memberToEdit.Ocupation = Ocupation;
            memberToEdit.ShirtNumber = ShirtNumber;

            
        }

        public void RemoveMember(MemberModel memberToDelete)
        {
            memberToDelete.Club.StaffList.Remove(memberToDelete);
            MemberList.Remove(memberToDelete);
        }

        public void LoadFromFile(ClubModel Club,int ID, string Name, Ocupation Ocupation, int ShirtNumber)
        {
            MemberModel newMember = new MemberModel();

            newMember.ID = ID;
            newMember.Club = Club;
            newMember.Name = Name;
            newMember.Ocupation = Ocupation;
            newMember.ShirtNumber = ShirtNumber;

            MemberList.Add(newMember);
            Club.StaffList.Add(newMember);
        }
    }
}
