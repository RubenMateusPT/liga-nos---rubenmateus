﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelClasses
{
    public class ClubModel : CommonInformationKeywords
    {

        public string StadiumName { get; set; }
        public string Description { get; set; }
        public string LogoLocation { get; set; }

        public List<MemberModel> StaffList { get; set; }
        public List<GameMatchModel> GameMatchHome { get; set; }
        public List<GameMatchModel> GameMatchAway { get; set; }



        public string ClubInformation { get { return $"{ID}:{Name}; {StadiumName};{Description}; {LogoLocation}"; } }

    }
}
