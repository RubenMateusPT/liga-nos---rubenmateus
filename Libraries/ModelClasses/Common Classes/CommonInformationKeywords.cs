﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelClasses
{
   public  class CommonInformationKeywords
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
