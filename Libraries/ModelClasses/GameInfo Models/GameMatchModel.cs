﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelClasses
{
    public class GameMatchModel : CommonGamematchKeywordsModel
    {
        public ClubModel MyClub { get; set; }
        public ClubModel RivalClub { get; set; }
        public int GoalsScored { get; set; }
        public bool PlayedHome { get; set; }

        public string ClubInformation { get { return $"{MyClub.Name} VS {RivalClub.Name} : {GoalsScored}"; } }

    }
}
