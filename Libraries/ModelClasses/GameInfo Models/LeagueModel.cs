﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelClasses
{
    public class LeagueModel : CommonGamematchKeywordsModel
    {
        public List<SetModel> GamesList { get; set; }
    }
}
