﻿namespace ModelClasses
{
   
    public class MemberModel : CommonInformationKeywords
    {
        public ClubModel Club { get; set; }
        public Ocupation Ocupation { get; set; }
        public int ShirtNumber { get; set; }


        public string MemberInformation { get { return $"{ID}:{Name}; {Club.Name}; {Ocupation}; {ShirtNumber}"; } }

    }
}
