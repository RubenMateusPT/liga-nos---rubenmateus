﻿namespace Library
{
    using System.Collections.Generic;
    using System.IO;

    public class ctrlClub
    {
        public List<tblClub> ClubList { get; private set; } //List of all clubs made

        /// <summary>
        /// Controller of Clubs
        /// </summary>
        public ctrlClub()
        {
            ClubList = new List<tblClub>();
        }

        /// <summary>
        /// Creates a New Club, and adds to the list of known Clubs
        /// </summary>
        /// <param name="Name">Name of the Club</param>
        /// <param name="Stadium">Name of the Stadium of Club</param>
        /// <param name="Description">Small Description of Club</param>
        /// <param name="LogoLocation">The location of the club logo image</param>
        public void NewClub(string Name, string Stadium, string Description, string LogoLocation)
        {
            int newID = 0;

            //generates an ID
            foreach(tblClub club in ClubList)
            {
                if(newID <= club.ID)
                {
                    newID = club.ID;
                }
            }

            tblClub newClub = new tblClub(newID+1, Name, Stadium, Description, LogoLocation);

            ClubList.Add(newClub);
        }

        /// <summary>
        /// Edits an already existing club
        /// </summary>
        /// <param name="ClubToEdit">The club object that is to edit</param>
        /// <param name="Name">New club name</param>
        /// <param name="Stadium">New club stadium name</param>
        /// <param name="Description">New description for club</param>
        /// <param name="LogoLocation">The new Logo of club</param>
        public void EditClub(tblClub ClubToEdit,string Name,string Stadium, string Description, string LogoLocation)
        {
            //Deletes current Club logo to give space for another one
            if (!ClubToEdit.Name.Equals(Name) && !string.IsNullOrEmpty(ClubToEdit.LogoLocation))
            {
                File.Delete(ClubToEdit.LogoLocation);
            }

            ClubToEdit.Name = Name;
            ClubToEdit.Stadium = Stadium;
            ClubToEdit.Description = Description;
            ClubToEdit.LogoLocation = LogoLocation;
        }

        /// <summary>
        /// Removes the selected club
        /// </summary>
        /// <param name="ClubToRemove"> Club to be removed if possible</param>
        public void RemoveClub(tblClub ClubToRemove)
        {

            ClubToRemove.Enabled = false;

        }

        /// <summary>
        /// Activates the club on database
        /// </summary>
        /// <param name="ClubToRestore">Club to be enabled</param>
        public void RestoreClub(tblClub ClubToRestore)
        {
            ClubToRestore.Enabled = true;
        }

        /// <summary>
        /// Backups the club logo to a safe folder
        /// </summary>
        /// <param name="ClubToBackup"> The club Name to rename image</param>
        /// <param name="LogoOriginalLocation"> Where the image is located</param>
        /// <returns></returns>
        public string BackupLogo(string name, string LogoOriginalLocation)
        {
            
            string Destination = $@"./Resources/Images/Clubs/{name.ToUpper()}.png";

            // Makes a duplicate to Resources folder, if able
            if (!string.IsNullOrEmpty(LogoOriginalLocation))
                if(LogoOriginalLocation.ToUpper() != "openfiledialog".ToUpper() && LogoOriginalLocation.ToUpper() != "null".ToUpper())
                {
                    File.Copy(LogoOriginalLocation, Destination, true);
                }
            else
            {
                return "null";
            }


            return Destination;
        }
    }
}
