﻿namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;

    public class ctrlFixtures
    {
        ctrlDatabase Database;

        public void NewFixture(ctrlDatabase Database, tblSeason CurrentSeason)
        {
            this.Database = Database;

            int newID = 0;

            //generates an unique id based on current season
            foreach (tblFixtures fixture in CurrentSeason.Fixtures)
            {
                if (newID <= fixture.ID)
                {
                    newID = fixture.ID;
                }
            }

            // doesnt create fixture if season already at max fixtures
            if (newID < CurrentSeason.MaxFixtures)
            {
                tblFixtures newFixture = new tblFixtures(newID + 1);

                CurrentSeason.Fixtures.Add(newFixture);

                int gamesPerFixture = (CurrentSeason.MaxGamesMatches / CurrentSeason.MaxFixtures);


                CreateGameMatches(CurrentSeason, CurrentSeason.Fixtures.Last());

            }

            // when fixture created are the same as season max fixture, the season is finished
            if (CurrentSeason.Fixtures.Count == CurrentSeason.MaxFixtures)
            {
                CurrentSeason.Finished = true;
            }

        }

        /// <summary>
        /// Creates the gamematchs for current season and current fixture
        /// </summary>
        /// <param name="CurrentSeason">Latest season that is active</param>
        /// <param name="CurrentFixture">Latest fixture created</param>
        private void CreateGameMatches(tblSeason CurrentSeason, tblFixtures CurrentFixture)
        {
            while (CurrentFixture.Matches.Count != CurrentSeason.MaxGamesMatches / CurrentSeason.MaxFixtures)
            {
                foreach (tblClub homeclub in CurrentSeason.Clubs)
                {
                    foreach (tblClub awayclub in CurrentSeason.Clubs)
                    {
                        if (homeclub != awayclub && !ClubPlayedThisFixture(CurrentFixture, homeclub, awayclub) && !ClubPlayedThisSeason(CurrentSeason, homeclub, awayclub))
                        {
                            Database.MatchController.NewMatch(CurrentFixture, homeclub, awayclub);
                        }
                    }
                }
            }
        } 

        /// <summary>
        /// Verifies if match had already been made
        /// </summary>
        /// <param name="currentSeason">Latest Season that is Active</param>
        /// <param name="homeClub">Club that is playing Home</param>
        /// <param name="awayClub">Club that is playing Away</param>
        /// <returns></returns>
        private bool ClubPlayedThisSeason(tblSeason currentSeason, tblClub homeClub, tblClub awayClub)
        {
            foreach(tblFixtures fixture in currentSeason.Fixtures)
            {
                foreach(tblMatch match in fixture.Matches)
                {
                    if(match.HomeClub == homeClub && match.AwayClub == awayClub)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Verifies if Home club or Away club had already played this Fixture
        /// </summary>
        /// <param name="CurrentFixture">Latest fixture created</param>
        /// <param name="HomeClub">Club that is playing Home</param>
        /// <param name="AwayClub">Club that is playing Away</param>
        /// <returns></returns>
        private bool ClubPlayedThisFixture(tblFixtures CurrentFixture,tblClub HomeClub, tblClub AwayClub)
        {
            foreach(tblMatch match in CurrentFixture.Matches)
            {
                if(match.HomeClub == HomeClub || match.AwayClub == HomeClub || match.AwayClub == AwayClub || match.HomeClub == AwayClub)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
