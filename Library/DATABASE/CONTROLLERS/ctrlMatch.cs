﻿namespace Library
{
    using System;
    using System.Linq;
    using System.Threading;
    using static Library.CustomEnum;
    public class ctrlMatch
    {
        private GameState MatchResultHomeClub { get; set; }
        private GameState MatchResultAwayClub { get; set; }

        /// <summary>
        /// Creates a ner gameMatch
        /// </summary>
        /// <param name="CurrentFixture">Last fixture created from Last Active Season</param>
        /// <param name="HomeClub">Club that will play Home</param>
        /// <param name="AwayClub">Club that will play Away</param>
        public void NewMatch(tblFixtures CurrentFixture,tblClub HomeClub, tblClub AwayClub)
        {
            int newID = 0;

            //generates unique ID
            foreach (tblMatch match in CurrentFixture.Matches)
            {
                if (newID <= match.ID)
                {
                    newID = match.ID;
                }
            }

            // randomizes goal scores for each team
            Random rdn = new Random();
            int homegoal = rdn.Next(0, 6);
            Thread.Sleep(100);
            int awaygoal = rdn.Next(0, 6);


            //finds which team has won, lost or tied
            if (homegoal > awaygoal)
            {
                MatchResultHomeClub = GameState.Win;
                HomeClub.Statistics.Last().GamesWonHome++;
                HomeClub.Statistics.Last().SeasonPoints += 3;


                MatchResultAwayClub = GameState.Lose;
                AwayClub.Statistics.Last().GamesLostAway++;
            }
            else if (homegoal < awaygoal)
            {
                MatchResultHomeClub = GameState.Lose;
                HomeClub.Statistics.Last().GamesLostHome++;

                MatchResultAwayClub = GameState.Win;
                AwayClub.Statistics.Last().SeasonPoints += 3;
                AwayClub.Statistics.Last().GamesWonAway++;
            }
            else
            {
                MatchResultHomeClub = GameState.Tie;
                HomeClub.Statistics.Last().GamesTiedHome++;
                HomeClub.Statistics.Last().SeasonPoints++;

                MatchResultAwayClub = GameState.Tie;
                AwayClub.Statistics.Last().GamesTiedAway++;
                AwayClub.Statistics.Last().SeasonPoints++;
            }

            //adds the necessary statitisc to each club
            #region
            HomeClub.Statistics.Last().GamesPlayed++;
            AwayClub.Statistics.Last().GamesPlayed++;

            HomeClub.Statistics.Last().GoalsScoredHome += homegoal;
            HomeClub.Statistics.Last().GoalsConcededHome += awaygoal;


            AwayClub.Statistics.Last().GoalsScoredAway += awaygoal;
            AwayClub.Statistics.Last().GoalsConcededAway += homegoal ;
            #endregion


            tblMatch newMatch = new tblMatch(newID + 1, HomeClub, AwayClub, homegoal, awaygoal, MatchResultHomeClub, MatchResultAwayClub);

            //adds the gamematch to current fixture
            CurrentFixture.Matches.Add(newMatch);

            
        }

    }
}
