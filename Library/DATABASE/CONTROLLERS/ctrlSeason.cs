﻿namespace Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    public class ctrlSeason
    {
        public List<tblSeason> Seasons { get; set; }

        /// <summary>
        /// Season controls (commands)
        /// </summary>
        public ctrlSeason()
        {
            Seasons = new List<tblSeason>();
        }

        /// <summary>
        /// Creates a new season for database
        /// </summary>
        /// <param name="Database">Database that is being currently used</param>
        public void NewSeason(ctrlDatabase Database)
        {
            int newID = 0;

            // generates unique id
            foreach (tblSeason season in Seasons)
            {
                if (newID <= season.ID)
                {
                    newID = season.ID;
                }
            }

            tblSeason newSeason = new tblSeason(newID + 1);

            //adds created season to database
            Seasons.Add(newSeason);

            CreateSeason(Database);
        }

        /// <summary>
        /// Starts creating the season to be operated
        /// </summary>
        /// <param name="Database">Database that is being currently used</param>
        private void CreateSeason(ctrlDatabase Database)
        {
            tblSeason CurrentSeason = Database.SeasonController.Seasons.Last();

            ChooseSeasonTeams(Database,CurrentSeason);

            FindMaxFixtures(CurrentSeason);

            FindMaxGameMatches(CurrentSeason);
            
        }

        /// <summary>
        /// Chooses on random, the team that will play in the new season (multiples of 4)
        /// </summary>
        /// <param name="Database">Current Database</param>
        /// <param name="CurrentSeason">Newly created season</param>
        private void ChooseSeasonTeams(ctrlDatabase Database, tblSeason CurrentSeason)
        {
            tblClub clubToAdd = null;
            int SeasonMaxClubs = Database.ClubController.ClubList.Count();

            //Finds the needed number of clubs to start season
            while (SeasonMaxClubs % 4 != 0)
            {
                SeasonMaxClubs--;
            }

            // searches and chooses randomly clubs to add to season (no repetition)
            for (int i = 0; i < SeasonMaxClubs; i++)
            {
                do {
                    Random rdn = new Random();
                    int choise = rdn.Next(0, Database.ClubController.ClubList.Count);
                    Thread.Sleep(100);

                    clubToAdd = Database.ClubController.ClubList.ElementAt(choise);
                    

                } while (ClubExists(CurrentSeason,clubToAdd) || !clubToAdd.Enabled);

                CurrentSeason.Clubs.Add(clubToAdd);
                Database.StatsControler.newStatistics(CurrentSeason, clubToAdd);

            }

        }

        /// <summary>
        /// Verifies if Club Selected is already in Curresnt Season List
        /// </summary>
        /// <param name="CurrentSeason">Latest Season created</param>
        /// <param name="ClubChoosen">Club to verify</param>
        /// <returns></returns>
        private bool ClubExists(tblSeason CurrentSeason,tblClub ClubChoosen)
        {
            foreach (tblClub club in CurrentSeason.Clubs)
            {
                if(club == ClubChoosen)
                {
                    return true;
                }
                
            }

            return false;
        }

        /// <summary>
        /// Calculates the needed fixture for the season
        /// </summary>
        /// <param name="CurrentSeason">Latest created season</param>
        private void FindMaxFixtures(tblSeason CurrentSeason)
        {
            int NeededFixtures = (CurrentSeason.Clubs.Count() - 1) * 2;
            CurrentSeason.MaxFixtures = NeededFixtures;
        }

        /// <summary>
        /// Calculates the needed gamematches for the season
        /// </summary>
        /// <param name="CurrentSeason">Latest created season</param>
        private void FindMaxGameMatches(tblSeason CurrentSeason)
        {
            //use of combinations formula !n/p!(n*p)!

            int n = 1;
            const int p = 2;
            int np = 1;

            for(int i = CurrentSeason.Clubs.Count(); i >0; i--)
            {
                n *= i;
            }

            for(int i = (CurrentSeason.Clubs.Count() - p); i > 0; i--)
            {
                np *= i;
            }

            CurrentSeason.MaxGamesMatches = ((n / (p*np))*2);
        }

    }
}
