﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class ctrlStatistics
    {
        public ctrlDatabase Database { get; set; }

        /// <summary>
        /// Creates Statistics controler
        /// </summary>
        /// <param name="Database">Current Database</param>
        public ctrlStatistics(ctrlDatabase Database)
        {
            this.Database = Database;
        }

        /// <summary>
        /// Makes a new Statistic
        /// </summary>
        /// <param name="CurrentSeason">Latest created season</param>
        /// <param name="ClubToAdd">Club of latest created season List</param>
        public void newStatistics(tblSeason CurrentSeason,tblClub ClubToAdd)
        {

            tblStatistics newStats = new tblStatistics(CurrentSeason);

            ClubToAdd.Statistics.Add(newStats);
        }

        /// <summary>
        /// Updates all the stats of the club in the current season
        /// </summary>
        /// <param name="CurrentSeason">Latest created season</param>
        public void updateStatistics(tblSeason CurrentSeason)
        {
            
            //Calculates GamesLost,Tied,Won and goals Scored and Conceded
            foreach(tblClub club in CurrentSeason.Clubs)
            {
                tblStatistics CurrentStats = club.Statistics.Last();

                CurrentStats.GamesLost = CurrentStats.GamesLostAway + CurrentStats.GamesLostHome;
                CurrentStats.GamesTied = CurrentStats.GamesTiedAway + CurrentStats.GamesTiedHome;
                CurrentStats.GamesWon = CurrentStats.GamesWonAway + CurrentStats.GamesWonHome;

                CurrentStats.GoalsScored = CurrentStats.GoalsScoredHome + CurrentStats.GoalsScoredAway;
                CurrentStats.GoalsConceded = CurrentStats.GoalsConcededHome + CurrentStats.GoalsConcededAway;

            }

            //Creates a temporary list with all the club sorted by Points, then Goals Scored then Goals Conceded
            List<tblClub> SortedClubsByPoints = CurrentSeason.Clubs.OrderByDescending(club => club.Statistics.Last().SeasonPoints).ThenByDescending(club => club.Statistics.Last().GoalsScored).ThenByDescending(club => club.Statistics.Last().GoalsConceded).ToList();

            //Based on the list, gives the correct classification to each club
            int position = 1;

            foreach (tblClub sortedclub in SortedClubsByPoints)
            {

                foreach(tblClub club in CurrentSeason.Clubs)
                {
                    if(sortedclub == club)
                    {
                        club.Statistics.Last().Classification = position;
                    }
                }

                position++;
            }


        }

    }
}
