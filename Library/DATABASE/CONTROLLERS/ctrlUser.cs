﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    
    public class ctrlUser
    {
        public List<tblUsers> Users { get; set; }

        /// <summary>
        /// Creates USer controler
        /// </summary>
        public ctrlUser()
        {
            Users = new List<tblUsers>();
        }

        /// <summary>
        /// Creates a new User
        /// </summary>
        /// <param name="FavouriteClub">User favourite club (can be null)</param>
        public void NewUser(tblClub FavouriteClub)
        {
            tblUsers newUser = new tblUsers(FavouriteClub);

            Users.Add(newUser);
        }
    }
}
