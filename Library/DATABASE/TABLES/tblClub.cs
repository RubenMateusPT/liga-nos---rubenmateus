﻿namespace Library
{
    using System.Collections.Generic;


    public class tblClub
    {
        public int ID { get; set; } 
        public string Name { get; set; } 
        public string Stadium { get; set; } 
        public string Description { get; set; } 
        public string LogoLocation { get; set; } 
        public List<tblStatistics> Statistics { get; set; } // Club List of all it's statistics
        public bool Enabled { get; set; } // Is the club active on the database?

        /// <summary>
        /// New Club Model
        /// </summary>
        /// <param name="ID">Unique Identification Number</param>
        /// <param name="Name">Club Name</param>
        /// <param name="Stadium">Club Stadium Name</param>
        /// <param name="Description">Club Desciption</param>
        /// <param name="LogoLocation">Club Uploaded Logo</param>
        public tblClub(int ID,string Name,string Stadium, string Description, string LogoLocation)
        {
            this.ID = ID;
            this.Name = Name;
            this.Stadium = Stadium;
            this.Description = Description;
            this.LogoLocation = LogoLocation;
            Statistics = new List<tblStatistics>();
            Enabled = true;
        }
    }
}
