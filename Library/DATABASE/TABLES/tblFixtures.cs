﻿namespace Library
{
    using System.Collections.Generic;

    public class tblFixtures
    {
        public int ID { get; set; } 
        public List<tblMatch> Matches { get; set; } //List of all the fixture gamematches

        /// <summary>
        /// Makes a new fixture (jornada)
        /// </summary>
        /// <param name="ID"> Unique Identification Number</param>
        public tblFixtures(int ID)
        {
            this.ID = ID;
            Matches = new List<tblMatch>();
        }
    }
}
