﻿namespace Library
{
    using static Library.CustomEnum;
    public class tblMatch
    {
        public int ID { get; set; }
        public tblClub HomeClub { get; set; }
        public tblClub AwayClub { get; set; }
        public int HomeGoals { get; set; }
        public int AwayGoals { get; set; }
        public GameState HomeResult { get; set; }
        public GameState AwayResult { get; set; }

        /// <summary>
        /// Makes a new GameMatch
        /// </summary>
        /// <param name="ID">Unique Identification Number</param>
        /// <param name="HomeClub">The club that played in the home side</param>
        /// <param name="AwayClub">The club that played in the away side</param>
        /// <param name="HomeGoals">Home club scored goals</param>
        /// <param name="AwayGoals">Away club scored goals</param>
        /// <param name="HomeResult">Home club match status</param>
        /// <param name="AwayResult">Away club match status</param>
        public tblMatch(int ID,tblClub HomeClub, tblClub AwayClub, int HomeGoals, int AwayGoals, GameState HomeResult, GameState AwayResult)
        {
            this.ID = ID;
            this.HomeClub = HomeClub;
            this.AwayClub = AwayClub;
            this.HomeGoals = HomeGoals;
            this.AwayGoals = AwayGoals;
            this.HomeResult = HomeResult;
            this.AwayResult = AwayResult;
        }

        
    }
}
