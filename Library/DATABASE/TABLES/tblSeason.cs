﻿namespace Library
{
    using System.Collections.Generic;

    public class tblSeason
    {
        public int ID { get; set; }
        public bool Finished { get; set; } // Is the season completed?
        public int MaxFixtures { get; set; } // Max fixtures(jornadas) the season has
        public int MaxGamesMatches { get; set; } // Max gamematches the season has
        public List<tblFixtures> Fixtures { get; set; } // List of all maded fixtures
        public List<tblClub> Clubs { get; set; } // List of all participating clubs

        /// <summary>
        /// Makes a new Season
        /// </summary>
        /// <param name="ID">Unique Identification Number</param>
        public tblSeason(int ID)
        {
            this.ID = ID;
            Fixtures = new List<tblFixtures>();
            Clubs = new List<tblClub>();
            Finished = false;
        }
    }
}
