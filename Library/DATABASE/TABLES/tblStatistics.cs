﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class tblStatistics
    {
        #region Popreties
        public tblSeason Season { get; set; } // Season to work
        public int GamesPlayed { get; set; }
        public int GamesWon { get; set; }
        public int GamesWonHome { get; set; }
        public int GamesWonAway { get; set; }
        public int GamesTied { get; set; }
        public int GamesTiedHome { get; set; }
        public int GamesTiedAway { get; set; }
        public int GamesLost { get; set; }
        public int GamesLostHome { get; set; }
        public int GamesLostAway { get; set; }
        public int GoalsScored { get; set; }
        public int GoalsScoredHome { get; set; }
        public int GoalsScoredAway { get; set; }
        public int GoalsConceded { get; set; }
        public int GoalsConcededHome { get; set; }
        public int GoalsConcededAway { get; set; }
        public int SeasonPoints { get; set; }
        public int Classification { get; set; }
        #endregion

        /// <summary>
        /// Makes a new Statitics for club based on Season
        /// </summary>
        /// <param name="CurrentSeason">Receives the Current Season that is being played</param>
        public tblStatistics(tblSeason CurrentSeason)
        {
            this.Season = CurrentSeason;
            GamesPlayed = 0;
            GamesWonHome = 0;
            GamesWonAway = 0;
            GamesTiedHome = 0;
            GamesTiedAway = 0;
            GamesLostHome = 0;
            GamesLostAway = 0;
            GoalsScoredHome = 0;
            GoalsScoredAway = 0;
            GoalsConcededHome = 0;
            GoalsConcededAway = 0;
        }
    }
}
