﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class tblUsers
    {
        public tblClub FavouriteClub { get; set; }
        public bool AnsweredFavouritClub { get; set; } // has the user been asked about it's favourite club?

        /// <summary>
        /// Makes a one user only to save data of choices
        /// </summary>
        /// <param name="FavouriteClub">User favourite club choice</param>
        public tblUsers(tblClub FavouriteClub)
        {
            this.FavouriteClub = FavouriteClub;
        }
    }
}
