﻿namespace Library
{
    using System;
    using System.IO;
    using System.Linq;

    public class ctrlDatabase
    {
        #region Dabase Controllers
        public ctrlSeason SeasonController { get; set; }
        public ctrlFixtures FixtureController { get; set; }
        public ctrlMatch MatchController { get; set; }
        public ctrlClub ClubController { get; set; }
        public ctrlUser UserController { get; set; }
        public ctrlStatistics StatsControler { get; set; }
        #endregion

        public tblUsers CurrentUser { get; set; }

        /// <summary>
        /// Creates the database and the database controller
        /// </summary>
        public ctrlDatabase()
        {
            SeasonController = new ctrlSeason();
            FixtureController = new ctrlFixtures();
            MatchController = new ctrlMatch();
            ClubController = new ctrlClub();
            UserController = new ctrlUser();
            StatsControler = new ctrlStatistics(null);
        }

        /// <summary>
        /// Creates all necessery folder for the app to work
        /// </summary>
        public void CreateDirectories()
        {
            Directory.CreateDirectory(@".\Resources\TextFiles");
            Directory.CreateDirectory(@".\Resources\Images\Clubs");
        }

        /// <summary>
        /// Loads All Database Text Files
        /// </summary>
        public void LoadFile()
        {
            LoadClubs();

            LoadUsers();

            LoadSeason();

            LoadStatus();
        }

        /// <summary>
        /// Load all files relates to statistics (Must be last!)
        /// </summary>
        private void LoadStatus()
        {
            string statusFile = @"./Resources/TextFiles/ListOfClubStatistics.txt";

            StreamReader sr;

            if (File.Exists(statusFile))
            {
                sr = File.OpenText(statusFile);

                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] fields = line.Split('|');

                    tblClub ClubToWork = null;

                    //Search the club corresponding to loaded statistics
                    foreach(tblClub club in ClubController.ClubList)
                    {
                        if(club.ID == int.Parse(fields[0]))
                        {
                            ClubToWork = club;
                        }
                    }

                    //Searches the season corresponding to club statistc
                    foreach(tblSeason season in SeasonController.Seasons)
                    {
                        if (season.ID == int.Parse(fields[1]))
                        {
                            tblStatistics loadedStatus = new tblStatistics(season);
                            ClubToWork.Statistics.Add(loadedStatus);

                            loadedStatus.Classification = int.Parse(fields[3]);
                            loadedStatus.SeasonPoints = int.Parse(fields[2]);
                            loadedStatus.GamesPlayed = int.Parse(fields[19]);

                            loadedStatus.GamesLost = int.Parse(fields[16]);
                            loadedStatus.GamesLostAway = int.Parse(fields[18]);
                            loadedStatus.GamesLostHome = int.Parse(fields[17]);

                            loadedStatus.GamesTied = int.Parse(fields[13]);
                            loadedStatus.GamesTiedAway = int.Parse(fields[15]);
                            loadedStatus.GamesTiedHome = int.Parse(fields[14]);

                            loadedStatus.GamesWon = int.Parse(fields[10]);
                            loadedStatus.GamesWonAway = int.Parse(fields[12]);
                            loadedStatus.GamesWonHome = int.Parse(fields[11]);

                            loadedStatus.GoalsConceded = int.Parse(fields[7]);
                            loadedStatus.GoalsConcededAway = int.Parse(fields[9]);
                            loadedStatus.GoalsConcededHome = int.Parse(fields[8]);

                            loadedStatus.GoalsScored = int.Parse(fields[4]);
                            loadedStatus.GoalsScoredAway = int.Parse(fields[6]);
                            loadedStatus.GoalsScoredHome = int.Parse(fields[5]);

                            
                        }
                    }


                }

                sr.Close();
            }
        }
    
        /// <summary>
        /// Loads all files related to Seasons (Must be before Statitistics)
        /// </summary>
        private void LoadSeason()
        {
            string seasonFile = @"./Resources/TextFiles/ListOfSeasons.txt";

            StreamReader sr;

            if (File.Exists(seasonFile))
            {
                sr = File.OpenText(seasonFile);

                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] fields = line.Split('|');


                    tblSeason loadedSeason = new tblSeason(int.Parse(fields[0]));
                    loadedSeason.MaxFixtures = int.Parse(fields[1]);
                    loadedSeason.MaxGamesMatches = int.Parse(fields[2]);
                    loadedSeason.Finished = bool.Parse(fields[3]);

                    SeasonController.Seasons.Add(loadedSeason);

                    //Adds Saved Clubs from Seasons to Correct Season
                    for (int i = 4; i < fields.Length; i++)
                    {
                        foreach (tblClub club in ClubController.ClubList)
                        {
                            if(club.ID == int.Parse(fields[i]))
                            {
                                SeasonController.Seasons.Last().Clubs.Add(club);
                            }
                        }

                        }

                    LoadFixtures(loadedSeason);

                    
                    
                }

                sr.Close();
            }
        }

        /// <summary>
        /// Loads all files related to Fixtures 
        /// </summary>
        /// <param name="loadedSeason">Current loaded season to work with</param>
        private void LoadFixtures(tblSeason loadedSeason)
        {
            string fixtureFile = @"./Resources/TextFiles/ListOfFixtures.txt";

            StreamReader sr;

            if (File.Exists(fixtureFile))
            {
                sr = File.OpenText(fixtureFile);

                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    
                    string[] fields = line.Split('|');

                    if (loadedSeason.ID == int.Parse(fields[0]))
                    {
                        tblFixtures loadedFixture = new tblFixtures(int.Parse(fields[1]));

                        SeasonController.Seasons.Last().Fixtures.Add(loadedFixture);

                        LoadMatches(loadedSeason);

                    }

                }

                sr.Close();
            }


        }

        /// <summary>
        /// Loads all files related to GameMatches
        /// </summary>
        /// <param name="loadedSeason">Current loaded season to with</param>
        private void LoadMatches(tblSeason loadedSeason)
        {
            string matchesFile = @"./Resources/TextFiles/ListOfMatches.txt";

            StreamReader sr;

            if (File.Exists(matchesFile))
            {
                sr = File.OpenText(matchesFile);

                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {

                    string[] fields = line.Split('|');

                    if (loadedSeason.ID == int.Parse(fields[0]) && loadedSeason.Fixtures.Last().ID == int.Parse(fields[1]))
                    {
                        tblClub HomeClub = null;
                        tblClub AwayClub = null;
                        foreach(tblClub club in ClubController.ClubList)
                        {
                            if(club.ID == int.Parse(fields[3]))
                            {
                                HomeClub = club;
                            }
                            if(club.ID == int.Parse(fields[6])){
                                AwayClub = club;
                            }
                        }

                        CustomEnum.GameState HomeStatus;
                        CustomEnum.GameState AwayStatus;

                        Enum.TryParse(fields[5], out HomeStatus);
                        Enum.TryParse(fields[8], out AwayStatus);
                        tblMatch loadedMatch = new tblMatch(int.Parse(fields[2]), HomeClub, AwayClub, int.Parse(fields[4]), int.Parse(fields[7]),HomeStatus,AwayStatus);


                        //adds the game match to the latest season loaded and latest fixture loaded
                        SeasonController.Seasons.Last().Fixtures.Last().Matches.Add(loadedMatch);

                    }

                }

                sr.Close();
            }
        }

        /// <summary>
        /// Loads all files related to Clubs (Must be first!)
        /// </summary>
        private void LoadClubs()
        {
            string clubFile = @"./Resources/TextFiles/ListOfClubs.txt";

            StreamReader sr;

            if (File.Exists(clubFile))
            {
                sr = File.OpenText(clubFile);

                string line = string.Empty;


                while ((line = sr.ReadLine()) != null)
                {
                    string[] fields = line.Split('|');

                    string description = fields[3];

                    //Converts linebreak to usable linebreaks
                    if (fields[3].Contains("\\r\\n"))
                    {
                        description = fields[3].Replace("\\r\\n", Environment.NewLine);
                    }

                    tblClub loadedClub = new tblClub(int.Parse(fields[0]), fields[1], fields[2], description, fields[4]);

                    loadedClub.Enabled = bool.Parse(fields[5]);

                    ClubController.ClubList.Add(loadedClub);
                }

                sr.Close();
            }
        }

        /// <summary>
        /// Loads all files related to Users (Must be after Clubs)
        /// </summary>
        private void LoadUsers()
        {
            string userFile = @"./Resources/TextFiles/ListOfUsers.txt";

            StreamReader sr;

            if (File.Exists(userFile))
            {
                sr = File.OpenText(userFile);

                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] fields = line.Split('|');

                    tblClub FavouriteClub = null;

                    //Checks if avaible, loaded saved club to user
                    if (fields[0].ToUpper() != "null".ToUpper())
                    {
                        foreach (tblClub club in ClubController.ClubList)
                        {
                            if (club.ID == int.Parse(fields[0]))
                            {
                                FavouriteClub = club;
                            }
                        }
                    }

                    tblUsers loadedUser = new tblUsers(FavouriteClub);
                    loadedUser.AnsweredFavouritClub = bool.Parse(fields[1]);

                    UserController.Users.Add(loadedUser);
                }

                sr.Close();
            }
        }

        /// <summary>
        /// Saves all Database Data into TextFiles
        /// </summary>
        public void SaveFile()
        {
            SaveClubs();
            SaveUsers();
            SaveSeason();
            SaveStatistics();
        }

        /// <summary>
        /// Saves all information of Season to files
        /// </summary>
        private void SaveSeason()
        {
            string seasonFile = @"./Resources/TextFiles/ListOfSeasons.txt";

            StreamWriter sw = new StreamWriter(seasonFile, false);

            if (!File.Exists(seasonFile))
            {
                sw = File.CreateText(seasonFile);
            }

            foreach (tblSeason seasonToSave in SeasonController.Seasons)
            {
                sw.Write($"{seasonToSave.ID.ToString()}|{seasonToSave.MaxFixtures.ToString()}|{seasonToSave.MaxGamesMatches.ToString()}|{seasonToSave.Finished.ToString()}");

                foreach(tblClub club in seasonToSave.Clubs)
                {
                    sw.Write($"|{club.ID}");
                }

                sw.WriteLine();
            }

            SaveFixture();

            sw.Close();
        }

        /// <summary>
        /// Saves all information of Fixtures to files
        /// </summary>
        private void SaveFixture()
        {
            string fixtureFile = @"./Resources/TextFiles/ListOfFixtures.txt";

            StreamWriter sw = new StreamWriter(fixtureFile, false);

            if (!File.Exists(fixtureFile))
            {
                sw = File.CreateText(fixtureFile);
            }
            
            foreach(tblSeason season in SeasonController.Seasons) {
                foreach (tblFixtures fixture in season.Fixtures)
                {
                    sw.WriteLine($"{season.ID.ToString()}|{fixture.ID.ToString()}");

                } }

            SaveMatches();

            sw.Close();
        }

        /// <summary>
        /// Saves all information of GameMatches to files
        /// </summary>
        private void SaveMatches()
        {
            string matchesFiles = @"./Resources/TextFiles/ListOfMatches.txt";

            StreamWriter sw = new StreamWriter(matchesFiles, false);

            if (!File.Exists(matchesFiles))
            {
                sw = File.CreateText(matchesFiles);
            }

            foreach (tblSeason season in SeasonController.Seasons)
            {
                foreach (tblFixtures fixture in season.Fixtures)
                {
                    foreach (tblMatch match in fixture.Matches)
                    {
                        sw.WriteLine($"{season.ID.ToString()}|{fixture.ID.ToString()}|{match.ID.ToString()}|" +
                            $"{match.HomeClub.ID.ToString()}|{match.HomeGoals.ToString()}|{match.HomeResult.ToString()}|" +
                            $"{match.AwayClub.ID.ToString()}|{match.AwayGoals.ToString()}|{match.AwayResult.ToString()}");

                    }
                }
            }

            sw.Close();
        }

        /// <summary>
        /// Saves all information Of Clubs to files
        /// </summary>
        private void SaveClubs()
        {
            string clubFile = @"./Resources/TextFiles/ListOfClubs.txt";

            StreamWriter sw = new StreamWriter(clubFile, false);

            if (!File.Exists(clubFile))
            {
                sw = File.CreateText(clubFile);
            }

            foreach (tblClub clubToSave in ClubController.ClubList)
            {
                sw.Write($"{clubToSave.ID.ToString()}|{clubToSave.Name}|{clubToSave.Stadium}|");

                string descp = clubToSave.Description.Replace(Environment.NewLine,@"\r\n");
                sw.Write($"{descp}");

                sw.WriteLine($"|{clubToSave.LogoLocation}|{clubToSave.Enabled.ToString()}");
            }

            sw.Close();
        }

        /// <summary>
        /// Saves all information of Statistics to files
        /// </summary>
        private void SaveStatistics()
        {
            string statisticsFile = @"./Resources/TextFiles/ListOfClubStatistics.txt";

            StreamWriter sw = new StreamWriter(statisticsFile, false);

            if (!File.Exists(statisticsFile))
            {
                sw = File.CreateText(statisticsFile);
            }
            foreach (tblClub club in ClubController.ClubList)
            {
                foreach (tblStatistics stats in club.Statistics)
                {
                    sw.WriteLine($"{club.ID.ToString()}|{stats.Season.ID.ToString()}|{stats.SeasonPoints.ToString()}|{stats.Classification.ToString()}|" +
                        $"{stats.GoalsScored.ToString()}|{stats.GoalsScoredHome.ToString()}|{stats.GoalsScoredAway.ToString()}|" +
                        $"{stats.GoalsConceded.ToString()}|{stats.GoalsConcededHome.ToString()}|{stats.GoalsConcededAway.ToString()}|" +
                        $"{stats.GamesWon.ToString()}|{stats.GamesWonHome.ToString()}|{stats.GamesWonAway.ToString()}|" +
                        $"{stats.GamesTied.ToString()}|{stats.GamesTiedHome.ToString()}|{stats.GamesTiedAway.ToString()}|" +
                        $"{stats.GamesLost.ToString()}|{stats.GamesLostHome.ToString()}|{stats.GamesLostAway.ToString()}|" +
                        $"{stats.GamesPlayed.ToString()}");

                }
            }

            sw.Close();
        }

        /// <summary>
        /// Saves all information of user to file
        /// </summary>
        private void SaveUsers()
        {
            string userFile = @"./Resources/TextFiles/ListOfUsers.txt";

            StreamWriter sw = new StreamWriter(userFile, false);

            if (!File.Exists(userFile))
            {
                sw = File.CreateText(userFile);
            }

            foreach (tblUsers userToSave in UserController.Users)
            {
                if (userToSave.FavouriteClub == null)
                {
                    sw.WriteLine($"null|{userToSave.AnsweredFavouritClub.ToString()}");
                }
                else
                {
                    sw.WriteLine($"{userToSave.FavouriteClub.ID}|{userToSave.AnsweredFavouritClub.ToString()}");
                }
            }

            sw.Close();
        }
    }
}
